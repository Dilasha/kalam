<div class="row" ng-app="app" ng-controller="ctrl">
	<h5>List : {{listbooks[0].listtitle}}</h5>	
	<div class="columns eight">
		<div class="row" id="listbook-{{listbook.booklistId}}" ng-repeat="listbook in listbooks">
			<div class="row">
				<div class="columns eight">
					<h6>{{listbook.title}}</h6>				
				</div>
				<div class="columns four right">
					<a href="<?php echo base_url(); ?>welcome/book_details/{{listbook.bookId}}" class="button button-default"><i class="fa fa-list"></i> View Details</a>	
					<button ng-click="deleteListbook(listbook)" class="button button-default"><i class="fa fa-trash-o"></i> Remove from list</button>				
				</div>
			</div>
			<div class="row">
				<div class="columns two">
					<table class="u-full-width">
						<tr>
							<td><img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{listbook.cover}}" alt=""></td>
						</tr>
					</table>
				</div>
				<div class="columns ten">
					<table class="row u-full-width">
						<tr>
							<td colspan="2"><strong>Title:</strong></td>
							<td>{{listbook.title}}</td>
						</tr>
						<tr>
							<td colspan="2"><strong>Author:</strong></td>
							<td>{{listbook.author}}</td>
						</tr>
						<tr>
							<td colspan="2"><strong>Genre:</strong></td>
							<td>{{listbook.genre}}</td>
						</tr>
						<tr>
							<td><strong>Synopsis:</strong></td>
							<td colspan="5">{{listbook.blurb|limitTo:500}}...</td>
						</tr>
					</table>
				</div>
			</div>		
		</div>
	</div>
	<div class="columns offset-by-one three sidebar">
		<h6>My Lists</h6>
		<ul>
			<li ng-repeat="list in lists">
				<a href="<?php echo base_url(); ?>welcome/view_list/{{list.listId}}">{{list.listtitle}}</a>
			</li>
		</ul>
	</div>
</div>
<script type="text/javascript">
	var userId='<?php echo $user; ?>';
	var listId = '<?php echo $this->uri->segment(3); ?>';
	var app=angular.module('app',[]);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var listbooks=[];
		$http.get('<?php echo base_url(); ?>users/get_listbooks', {
			params: {listId: listId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.listbooks = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		var lists=[];
		$http.get('<?php echo base_url(); ?>users/get_lists',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.lists=data;
		}).error(function(data){
			console.log(data);
		});

		/*$interval(function() {
			listbooks = []
			$http.get("<?php echo base_url(); ?>users/get_books")
			.then(function(response) {
				console.log(response);
				listbooks = response.data;
				console.log(listbooks);
				$scope.listbooks = listbooks
			});
		}, 3000);*/
		$scope.deleteListbook = function(listbook) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/delete_listbook',
				data: $.param({
					'booklistId': listbook.booklistId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#listbook-" + listbook.booklistId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>