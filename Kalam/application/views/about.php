<h5>About</h5>
<div class="row">
	<div class="columns four">
		<img class="u-max-full-width" src="<?php echo base_url(); ?>assets/images/logo.png" alt="">
	</div>
	<div class="columns six offset-by-one">
		<p>
			<strong>Kalam</strong> is a platform for Nepali Literature enthusiasts to discuss and share their love for Nepali books.
			Find your next read or express your thoughts on a book you just recently finished.
		</p>
		<p>
			We do not claim to have written of have any association in the writing, plot or publication of the books listed unless stated wise. All rights belong to the
			respective authors. We simply showcase them for the world to find.
		</p>
		<p>
			<strong>A celebration of Nepali Literature</strong>
		</p>
		<p>
			For any inquiries or complaints please visit our <a href="<?php echo base_url(); ?>welcome/contact" title="">Contact page</a>.
		</p>
	</div>
</div>