<h5>News &amp; Events</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="row">
		<div class="columns eight">
			<div class="list-box" id="rowb-{{newspost.newspostId}}" ng-repeat="newspost in newsposts">
				<div class="row">
					<div class="columns eight">
						<h6>{{newspost.heading}}</h6>
					</div>
					<div class="columns four right">
						<a href="<?php echo base_url(); ?>welcome/newspost_full/{{newspost.newspostId}}" class="button">Read More</a>
					</div>
				</div>	
				<div class="row body" ng-bind-html="newspost.text | linebreak | to_trusted">
					{{newspost.text}}
				</div>
				<hr />			
				<div class="row">
				<div class="columns six">
						<strong>Created by:</strong> {{newspost.firstname}} {{newspost.lastname}}
					
				</div>
				<div class="columns six right">
						<strong>Created on:</strong> {{newspost.publishedOn}}
				</div>
				</div>	
			</div>
		</div>			
		<div class="columns offset-by-one three sidebar">
			<h5>Archive</h5>
			<ul>
				<li id="archive-{{newspost.newspostId}}" ng-repeat="newspost in newsposts">
					<a href="#rowb-{{newspost.newspostId}}">{{newspost.heading}} </a>
				</li>
			</ul>
		</div>
	</div>
</div>
<script>
	var app= angular.module('app', []);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
	var newsposts=[];
		$http.get('<?php echo base_url(); ?>users/get_newsposts', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.newsposts = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			newsposts = []
			$http.get("<?php echo base_url(); ?>users/get_newsposts")
			.then(function(response) {
				console.log(response);
				newsposts = response.data;
				console.log(newsposts);
				$scope.newsposts = newsposts
			});
		}, 3000);*/
	}]);

	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>	