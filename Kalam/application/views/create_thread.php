<h5>Create Thread</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="threadcreate" ng-submit="submit(threadcreate.$valid)" novalidate="">
		<label>Title</label>
		<span ng-messages="threadcreate.title.$dirty && threadcreate.title.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread title.</span>
			<span class="invalid" ng-message="maxlength"><i class="fa fa-close"></i> The thread title cannot exceed 50 characters.</span>
		</span>
		<input  type="text" name="title" class="u-full-width" ng-model="title" ng-maxlength="100" required />


		<label class="form-label">Description</label>
		<span ng-messages="threadcreate.description.$dirty && threadcreate.description.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread description.</span>
			<span class="invalid" ng-message="minlength"><i class="fa fa-close"></i> The thread description must include atleast 10 characters.</span>
		</span>
		<textarea name="description" class="u-full-width" ng-model="description" ng-minlength="10" required></textarea>
		<br />

		<input type="submit" value="Create Thread" ng-disabled="threadcreate.$invalid || threadcreate.$pending" class="button u-pull-right" />
	</form>
</div>
<script type="text/javascript">

	$('[name=tags]').tagsInput();
	var app = angular.module('app',['ngMessages', 'ngSanitize']);
	app.controller('ctrl', function($scope, $http) {
		$scope.message="";

		$scope.submit = function(isValid) {
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>users/add_thread',
					data: $.param({
						'title': $scope.title,
						'description': $scope.description
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data, status, headers, config) {
					console.log(data);
					if (data == "success") {
						$scope.message="<div class='success'><i class='fa fa-check-circle'></i>  Success, your thread was created!</div>";
						$scope.title = "";
						$scope.description = "";
						$scope.threadcreate.$setPristine();
					} else {
						$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					}
				}).error(function(response) {
					console.log(response);
					$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					swal('Oops!', 'There were some errors in your form', 'error')
				});
			}else{
				swal('Oops!', 'There were some errors in your form', 'error')
			}
		}
	});
</script>