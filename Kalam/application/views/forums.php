<h5>Forums</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="row">
		<div class="columns eight">
			<div class="list-box" id="rowb-{{thread.threadId}}" ng-repeat="thread in threads">
				<div class="row">
					<div class="columns eight">
						<h6>{{thread.title}}</h6>
					</div>
					<div class="columns four right">
						<a href="<?php echo base_url(); ?>welcome/forum_thread/{{thread.threadId}}" class="button">Enter Thread</a>
					</div>
				</div>	
				<div class="row body">
					{{thread.description}}
				</div>
				<hr />			
				<div class="row">
					<div class="columns eight">
						<strong>Created by:</strong> {{thread.firstname}} {{thread.lastname}}
					</div>
					<div class="columns four right">
						<strong>Created on:</strong> {{thread.createdOn}}
					</div>
				</div>	
			</div>
		</div>
		<div class="columns offset-by-one three sidebar">
			<h6>Recently Active</h6>
			<ul>
				<li id="archive-{{thread.threadId}}" ng-repeat="thread in threads|orderBy:thread.lastActive">
					<a href="#rowb-{{thread.threadId}}">{{thread.title}} </a>
				</li>
			</ul>
		</div>
	</div>
	
</div>
<script>
	var app= angular.module('app', []);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var threads=[];
		$http.get('<?php echo base_url(); ?>users/get_threads', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.threads = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		$interval(function() {
			threads = []
			$http.get("<?php echo base_url(); ?>users/get_threads")
			.then(function(response) {
				console.log(response);
				threads = response.data;
				console.log(threads);
				$scope.threads = threads
			});
		}, 3000);

	}]);
</script>	