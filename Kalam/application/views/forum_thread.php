<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="columns ten">
		<div  id="rown-{{thread.threadId}}" ng-repeat="thread in threads|limitTo:1">
			<h5>{{thread.title}}</h5>
			<p>{{thread.description}}</p>
			<hr />			
			<strong>Author:</strong> {{thread.firstname}} {{thread.lastname}} on {{thread.createdOn}}
		</div>

		<hr />
		<div class="row">
			<h6>replies</h6>
			<div class='row' id="rowc-{{reply.replyId}}" ng-repeat="reply in replies">
				<div class="columns one">
					<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{reply.photo}}" alt="">
				</div>
				<div class="columns eight">
					<strong>{{reply.firstname}} {{reply.lastname}}</strong> says..
					<p ng-bind-html="reply.reply | linebreak | to_trusted"></p>
				</div>
			</div>
			<?php if ($this->session->userdata('sessiondata')!=null): ?>
				<div class="row">
					<form name="replyadd" ng-submit="reply(replyadd.$valid)" novalidate="">
							<!-- <textarea name="text" ng-model="text" class="u-full-width" placeholder="Add a reply" required=""></textarea>
						--><text-angular placeholder="Add a review" class="u-full-width" name="text" ng-model="text" required /></text-angular>
						<input type="submit" ng-disabled="replyadd.$invalid" class="button u-pull-right" value="Post" />
					</form>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	var app=angular.module('app',['textAngular' ,'ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		$scope.threadId='<?php echo $this->uri->segment(3); ?>';
		var threads=[];
		$http.get('<?php echo base_url(); ?>users/get_threads', {
			params: {threadId: $scope.threadId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.threads = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		var replies=[];
		$http.get('<?php echo base_url(); ?>users/get_replies', {
			params: {threadId: $scope.threadId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.replies = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.reply=function(){
			console.log('done');
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/add_reply',
				data: $.param({
					'threadId':$scope.threadId,
					'reply':$scope.text
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data == "success") {
					$scope.text="";
					$scope.replyadd.$setPristine();
				} else {
					swal('Oops!','There was an error processing your request.')
				}
			}).error(function(response) {
				console.log(response);
				swal('Oops!', 'There were some errors in your form', 'error')
			});
		}
		$interval(function() {
			replies = [];
			$http.get('<?php echo base_url(); ?>users/get_replies', {
				params: {threadId: $scope.threadId},
				header: {
					'Content-Type': 'application/json; charset=UTF-8'
				}
			}).
			success(function(data) {
				console.log(data);
				$scope.replies = data;
			});
		}, 2000);
	}]);

	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>