<script src="<?php echo base_url(); ?>assets/js/autocomplete.js"></script>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-init="rating = star.rating + 1"></div>
	<div star-rating rating-value="rating" data-max="5"></div>

	<input auto-complete ui-items="names" ng-model="selected">
	selected = {{selected}}

</div>
<script type="text/javascript">
	var app = angular.module('app', ['autocomplete']);

	app.controller('ctrl', function($scope, $http) {
		$scope.adata=[];
		$http.get('<?php echo base_url(); ?>users/get_books', {
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.books = data;
			for(var i = 0; i < $scope.books.length; i++){
				$scope.seperate=$scope.books[i].tags.split(',');
				for(var j = 0; j < $scope.seperate.length; j++)
					$scope.adata.push($scope.seperate[j]);
			}
			console.log($scope.adata);
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.updateTag = function(typed){
			$scope.newmovies = MovieRetriever.getmovies(typed);
			$scope.newmovies.then(function(data){
				$scope.movies = data;
			});
		}
		$scope.rating = 5;
	});
	app.directive('starRating',	function() {
		return {
			template: '<ul class="starRating">' + '   <li ng-repeat="star in stars" ng-class="star" ng-click="changeRating($index)">' + '\u2605' + '</li>' + '</ul>',
			scope: {
				ratingValue: '=',
				max: '=',
				onStarRating: '&'
			},
			link: function(scope, elem, attrs) {
				var updateRating = function() {
					scope.stars = [];
					for (var i = 0; i < scope.max; i++) {
						scope.stars.push({
							filled: i < scope.ratingValue
						});
					}
				};
				scope.changeRating = function(index) {
					scope.ratingValue = index + 1;
					scope.onStarRating({
						rating: index + 1
					});
				};
				scope.$watch('ratingValue', function(orating, nrating) {
					if (nrating) {
						updateRating();
					}
				});
			}
		};
	});
</script>