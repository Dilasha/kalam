<h5>Edit Newspost</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="newspostedit" ng-submit="update(newspostedit.$valid)" novalidate="">
		<label class="form-label">Heading</label>   
		<span ng-messages="newspostedit.heading.$dirty && newspostedit.heading.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the newspost heading.</span>
		</span>
		<input  type="text" name="heading" class="u-full-width" ng-model="heading" required />

		<label class="form-label">Text</label>
		<span ng-messages="newspostedit.text.$dirty && newspostedit.text.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the newspost text.</span>
		</span>
		<text-angular class="u-full-width" name="text" ng-model="text" required /></text-angular>
		<br />
		<input type="submit" value="Save Changes" ng-disabled="newspostedit.$invalid || newspostedit.$pending" class="button u-pull-right" />
	</form>
</div>
<script type="text/javascript">
	$('[name=tags]').tagsInput();
	var newspostId= '<?php echo $this->uri->segment(3); ?>';
	var app=angular.module('app',['ngMessages', 'ngSanitize', 'textAngular']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		var newsposts=[];
		$http.get('<?php echo base_url(); ?>admin/get_newsposts', {
			params:{ 'newspostId': newspostId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.newsposts = data;
			$scope.heading=$scope.newsposts[0].heading;
			$scope.text=$scope.newsposts[0].text;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.update=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>admin/update_newspost/'+newspostId,
					data: $.param({
						heading: $scope.heading,
						text: $scope.text
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data+"success");
					if(data != "success"){
						$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
					}else{
						$scope.message = "<div class='success'><i class='fa fa-check-circle'></i> Your changes have been saved!</div>";
						$scope.heading="";
						$scope.text="";
						$scope.newspostedit.$setPristine();
					}
				}).error(function(data){
					$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
				});
			}else{

				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, some fields in your form were invalid!</div>";
			}
		}
	}]);

	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>