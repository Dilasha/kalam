<h5>Messages</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
<a href="<?php echo base_url(); ?>admin/create_message" class="button u-pull-right"><i class="fa fa-plus"></i> Compose New</a>
	<table class="u-full-width">
		<thead>
			<th>Sender</th>
			<th>Subject</th>
			<th>Sent on</th>
			<th>View</th>
			<th>Delete</th>
		</thead>
		<tr id="message-{{message.messageId}}" ng-repeat="message in messages">
			<td>{{message.sender}}</td>
			<td>{{message.subject}}</td>
			<td>{{message.sentOn}}</td>
			<td><a class="button button-default" href="<?php echo base_url(); ?>admin/view_message/{{message.messageId}}"><i class="fa fa-envelope-o"></i> View</a></td>
			<td><a class="button button-default"` ng-click="deleteMessage(message)"><i class="fa fa-trash-o"></i> Delete</a></td>
		</tr>
	</table>

</div>
<script type="text/javascript">
	var app = angular.module('app',['ngMessages', 'ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){

		var messages=[];
		$http.get('<?php echo base_url(); ?>admin/get_messages', {
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.messages = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			messages = []
			$http.get("<?php echo base_url(); ?>admin/get_messages")
			.then(function(response) {
				console.log(response);
				messages = response.data;
				console.log(messages);
				$scope.messages = messages
			});
		}, 3000);*/

		$scope.deleteMessage = function(message) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_message',
				data: $.param({
					'messageId': message.messageId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#message-" + message.messageId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);

</script>