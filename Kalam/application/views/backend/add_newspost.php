<h5>Create newspost</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
  <div ng-bind-html="message"></div>
  <form name="newspostcreate" ng-submit="submit(newspostcreate.$valid)" novalidate="">
    <label class="form-label">Heading</label>   
    <span ng-messages="newspostcreate.heading.$dirty && newspostcreate.heading.$error">
      <span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the newspost heading.</span>
    </span>
    <input  type="text" name="heading" class="u-full-width" ng-model="heading" required />

    <label class="form-label">Text</label>
    <span ng-messages="newspostcreate.text.$dirty && newspostcreate.text.$error">
      <span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the newspost text.</span>
    </span>
    <text-angular class="u-full-width" name="text" ng-model="text" required /></text-angular>
    <br />
    <input type="submit" value="Create newspost" ng-disabled="newspostcreate.$invalid || newspostcreate.$pending" class="button u-pull-right" />
  </form>
</div>
<script type="text/javascript">
  var app = angular.module('app',['ngMessages', 'textAngular' ,'ngSanitize']);
  app.controller('ctrl', function($scope, $http) {
    $scope.message="";
    $scope.submit = function(isValid) {
      if(isValid){
        $http({
          method: 'post',
          url: '<?php echo base_url(); ?>admin/add_newspost',
          data: $.param({
            'heading': $scope.heading,
            'text': $scope.text
          }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data, status, headers, config) {
          console.log(data);
          if (data == "success") {
            $scope.message="<div class='success'><i class='fa fa-check-circle'></i>  Success, your newspost was created!</div>";
            $scope.heading = "";
            $scope.text = "";
            $scope.newspostcreate.$setPristine();
          } else {
            $scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
            $scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
          }
        }).error(function(response) {
          console.log(response);
          $scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
          swal('Oops!', 'There were some errors in your form', 'error')
        });
      }else{
        swal('Oops!', 'There were some errors in your form', 'error')
      }
    }
  });
</script>