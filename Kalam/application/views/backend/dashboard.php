<h5>Dashboard</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<p>You have <a href="<?php echo base_url(); ?>admin/messages">{{unread.length}} unread messages</a>  in your inbox.</p>
</div>
<script type="text/javascript">
	var app=angular.module('app',['ngSanitize']);
	app.controller('ctrl', ['$http', '$scope', '$interval', function($http, $scope, $interval){
		var unread=[];
		$http.get('<?php echo base_url(); ?>admin/get_unread', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.unread = data;
			$scope.un=$scope.unread.length;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		$interval(function() {
			unread = []
			$http.get("<?php echo base_url(); ?>admin/get_unread")
			.then(function(response) {
				console.log(response);
				unread = response.data;
				console.log(unread);
				$scope.unread = unread;
			});
		}, 2000);
	}]);
</script>