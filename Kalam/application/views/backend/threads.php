<h5>View Threads</h5>
<div class="row" ng-app="app" ng-controller="ctrl">	
	<div class="row search-sort">
		<div class="columns three">
			<label class="search-label">Search Key</label>
			<input type="text" class="u-full-width" name="keyword" data-ng-model="keyword"/>
		</div>
		<div class="columns three">
			<label class="search-label">Sort By</label>
			<select name="sort" class="u-full-width" ng-model="sort">
				<option value="">Default</option>
				<option value="title">Title</option>
				<option value="creator">Creator</option>
				<option value="createdOn">Date Created</option>
			</select>
		</div>
		<div class="columns six">
			<br />
			<a href="<?php echo base_url(); ?>admin/threads" class="button u-pull-right"><i class="fa fa-plus"></i> Create New</a>
		</div>
	</div>
	<div class="row list-box" id="thread-{{thread.threadId}}" ng-repeat="thread in threads| filter:keyword |orderBy:sort">
		<div class="row">
			<div class="columns eight">
				<h6>{{thread.title|uppercase}}</h6>
			</div>		
			<div class="columns four list-links right">
			<a href="<?php echo base_url(); ?>admin/edit_thread/{{thread.threadId}}" class="button">Edit</a>
				<button class="button" ng-click="deleteThread(thread)">Delete</button>
			</div>	
		</div>
		<div class="row">
			{{thread.description}}
		</div>
		<div class="row">
			<br />
			<strong>Created by:</strong> {{thread.firstname}} {{thread.lastname}} on:</strong> {{thread.createdOn}}
		</div>
	</div>
</div>
<script>
	var app=angular.module('app',[]);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var threads=[];
		$http.get('<?php echo base_url(); ?>admin/get_threads', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.threads = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			threads = []
			$http.get("<?php echo base_url(); ?>admin/get_threads")
			.then(function(response) {
				console.log(response);
				threads = response.data;
				console.log(threads);
				$scope.threads = threads
			});
		}, 3000);*/
		

		$scope.deleteThread = function(thread) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_thread',
				data: $.param({
					'threadId': thread.threadId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#thread-" + thread.threadId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>