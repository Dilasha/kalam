<h5>Edit Thread</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="threadedit" ng-submit="update(threadedit.$valid)" novalidate="">
		<label>Title</label>
		<span ng-messages="threadedit.title.$dirty && threadedit.title.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread title.</span>
			<span class="invalid" ng-message="maxlength"><i class="fa fa-close"></i> The thread title cannot exceed 50 characters.</span>
		</span>
		<input  type="text" name="title" class="u-full-width" ng-model="title" ng-maxlength="100" required />


		<label class="form-label">Description</label>
		<span ng-messages="threadedit.description.$dirty && threadedit.description.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread description.</span>
			<span class="invalid" ng-message="minlength"><i class="fa fa-close"></i> The thread description must include atleast 10 characters.</span>
		</span>
		<textarea name="description" class="u-full-width" ng-model="description" ng-minlength="10" required></textarea>



		<br />
		<input type="submit" value="Save Changes" ng-disabled="threadedit.$invalid || threadedit.$pending" class="button u-pull-right" />
	</form>
</div>
<script type="text/javascript">
	$('[name=tags]').tagsInput();
	var threadId= '<?php echo $this->uri->segment(3); ?>';
	var app=angular.module('app',['ngMessages', 'ngSanitize', 'textAngular']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		var threads=[];
		$http.get('<?php echo base_url(); ?>admin/get_threads', {
			params:{ 'threadId': threadId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.threads = data;
			$scope.title=$scope.threads[0].title;
			$scope.description=$scope.threads[0].description;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.update=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>admin/update_thread/'+threadId,
					data: $.param({
						title: $scope.title,
						description: $scope.description
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data+"success");
					if(data != "success"){
						$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
					}else{
						$scope.message = "<div class='success'><i class='fa fa-check-circle'></i> Your changes have been saved!</div>";
						$scope.title="";
						$scope.description="";
						$scope.threadedit.$setPristine();
					}
				}).error(function(data){
					$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
				});
			}else{

				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, some fields in your form were invalid!</div>";
			}
		}
	}]);
</script>