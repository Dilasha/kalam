<h5>Add User</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="useradd" ng-submit="submit(useradd.$valid)" novalidate>
		<div class="row">
			<div class="columns six">
				<label class="form-label">Last Name: </label>
				<span ng-messages="useradd.lastname.$dirty && useradd.lastname.$error">
					<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter the last name.</span>
				</span>
				<input name="lastname" type="text" ng-model="lastname" class="u-full-width" required/>

			</div>
			<div class="columns six">
				<label class="form-label">First Name: </label>
				<span ng-messages="useradd.firstname.$dirty && useradd.firstname.$error">
					<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter the first name.</span>
				</span>
				<input name="firstname" type="text" ng-model="firstname" class="u-full-width" required/>


			</div>
		</div>
		<div class="row">

			<label class="form-label">Email Address: </label>
			<span ng-messages="useradd.email.$dirty && useradd.email.$error">
				<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter an email address.</span>
				<span class="invalid" ng-message="email"> <i class="fa fa-close"></i> The email address you entered was invalid.</span>
				<span class="invalid" ng-message="isUnique"> <i class="fa fa-close"></i> This email address already exists in our database.</span>
			</span>
			<span class="valid" ng-hide="available"><i class='fa fa-check'></i> Email Address available.</span>
			<input name="email" type="email" ng-model="email" class="u-full-width" is-unique required/>
		</div>

		<div class="row">
			<div class="columns six">
				<label class="form-label">Password: </label>
				<span ng-messages="useradd.password.$dirty && useradd.password.$error">
					<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter a password.</span>
					<span class="invalid" ng-message="minlength"> <i class="fa fa-close"></i> Password must contain atleast 5 characters.</span>
				</span>
				<input name="password" type="password" ng-model="password" ng-minlength="5" class="u-full-width" required/>
			</div>
			<div class="columns six">
				<label class="form-label">Confirm Password: </label>
				<span ng-messages="useradd.cpassword.$dirty && useradd.cpassword.$error">
					<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter password again.</span>
					<span class="invalid" ng-message="compareTo"> <i class="fa fa-close"></i> There was a password mismatch</span>
				</span>
				<input name="cpassword" type="password" ng-model="cpassword" compare-to="password" class="u-full-width" required/>
			</div>
		</div>
		<div class="row">

			<label class="form-label">Date of Birth: </label>
			<span ng-messages="useradd.dob.$dirty && useradd.dob.$error">
				<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter date of birth.</span>
				<span class="invalid" ng-message="pattern"> <i class="fa fa-close"></i> The date format must be YYYY-MM-DD.</span>
			</span>
			<input name="dob" type="text"  ng-model="dob" ng-pattern="/^((\d{4})-(\d{2})-(\d{2})|(\d{2})\/(\d{2})\/(\d{4}))$/" class="u-full-width" required/>
		</div>
		<div class="row">
			<label class="form-label">Country: </label>
			<span ng-messages="useradd.country.$dirty && useradd.country.$error">
				<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter country of origin.</span>
			</span>
			<select class="u-full-width" name="country" ng-model="country" required>
				<?php 
				$this->load->helper('countries');
				$countries=get_countries();
				foreach ($countries as $value) {
					echo '<option value="'.$value.'">'.$value.'</option>';
				}
				?>			
			</select>
		</div>
		<div class="row">
			<label class="form-label">Role: </label>
			<span ng-messages="useradd.role.$dirty && useradd.role.$error">
				<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please provide user role.</span>
			</span>
			<select class="u-full-width" name="role" ng-model="role" required>
				<option value="0">Admin</option>
				<option value="1">User</option>			
			</select>
		</div>
		<input class="u-pull-right" type="submit" ng-disabled="useradd.$invalid" />
	</form>
</div>
<script type="text/javascript">
	jQuery('[name=dob]').datetimepicker({
		timepicker: false,
		format: 'Y-m-d'
	});
	var app= angular.module('app',['ngMessages','ngSanitize']);
	app.directive('isUnique', ['$http', function($http){
		return {
			require: 'ngModel',
			link: function (scope, elm, attrs, ctrl){
				elm.bind('focusout', function (blurEvent) {
					$http({
						method: 'POST', 
						url: '<?php echo base_url(); ?>admin/isUnique', 
						data: $.param({email: scope.email}),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					}).then(function(result) {
						console.log(result.data);
						if(result.data == "available"){
							scope.available=false;
							ctrl.$setValidity('isUnique', true);
						}
						else{
							ctrl.$setValidity('isUnique', false);
							scope.available=true;
						}
					});
				});
			}
		};
	}]);
	app.directive('compareTo',function() {
		return {
			require: "ngModel",
			scope: {
				passwordvalue: "=compareTo"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function(modelValue) {
					return modelValue == scope.passwordvalue;
				};
				scope.$watch("passwordvalue", function() {
					ngModel.$validate();
				});
			}
		};
	});
	app.controller('ctrl',function($scope, $http){
		$scope.available=true;
		$scope.submit=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>admin/add_user',
					data: $.param({
						lastname: $scope.lastname,
						firstname: $scope.firstname,
						email: $scope.email,
						password: $scope.password,
						dob: $scope.dob,
						country: $scope.country,
						role:$scope.role
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data+"success");
					if(data != "success"){
						$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
					}else{
						$scope.message = "<div class='success'><i class='fa fa-check-circle'></i> Success, user added!.</div>";
						$scope.lastname="";
						$scope.firstname="";
						$scope.email="";
						$scope.password="";
						$scope.cpassword="";
						$scope.dob="";
						$scope.country="";
						$scope.available=true;
						$scope.useradd.$setPristine();
					}
				}).error(function(data){
					$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
				});
			}else{
				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your registration credentials were invalid!</div>";
			}
		}
	});
</script>
