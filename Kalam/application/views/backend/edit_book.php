<h5>Edit Book</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<?php if($note!="") echo $note; ?>
	<div ng-bind-html="message"></div>
	<form ng-submit="submit(bookedit.$valid)" name="bookedit" novalidate="novalidate">
		<!-- <?php echo form_open_multipart('admin/add_book', array('name'=>'bookedit','novalidate'=>'novalidate')) ?> -->
		<label class="form-label">Title</label>
		<span ng-messages="bookedit.title.$dirty && bookedit.title.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread title.</span>
		</span>
		<input  type="text" name="title" class="u-full-width" ng-model="title" required />

		<label class="form-label">Blurb</label>
		<span ng-messages="bookedit.blurb.$dirty && bookedit.blurb.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the book blurb.</span>
			<span class="invalid" ng-message="minlength"><i class="fa fa-close"></i> The blurb must be atleast 10 characters long.</span>
		</span>
		<textarea name="blurb" class="u-full-width" ng-model="blurb" ng-minlength="10" required></textarea>


		<label class="form-label">Author</label>
		<span ng-messages="bookedit.author.$dirty && bookedit.author.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the author's name.</span>
		</span>
		<input type="text" name="author" class="u-full-width" ng-model="author" required />

		<label class="form-label">Genre</label>
		<span ng-messages="bookedit.genre.$dirty && bookedit.genre.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select a genre.</span>
		</span>
		<select class="u-full-width" name="genre" ng-model="genre" required>
			<?php 
			$this->load->helper('genre');
			$genres=get_genres();
			foreach ($genres as $value) {
				echo '<option value="'.$value.'">'.$value.'</option>';
			}
			?>			
		</select>

		<label class="form-label">Language</label>
		<span ng-messages="bookedit.language.$dirty && bookedit.language.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select the language.</span>
		</span>
		<select class="u-full-width" name="language" ng-model="language" required>
			<option value="Nepali">Nepali</option>
			<option value="English">English</option>
			<option value="Other">Other</option>
		</select>

		<label class="form-label">Publisher</label>
		<span ng-messages="bookedit.publisher.$dirty && bookedit.publisher.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the publisher's name.</span>
		</span>
		<input type="text" name="publisher" class="u-full-width" ng-model="publisher" required />

		<label class="form-label">Published Date: </label>
		<span ng-messages="signup.publishedOn.$dirty && signup.publishedOn.$error">
			<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your date of birth.</span>
			<span class="invalid" ng-message="pattern"> <i class="fa fa-close"></i> The date format must be YYYY-MM-DD.</span>
		</span>
		<input name="publishedOn" type="text"  ng-model="publishedOn" ng-pattern="/^((\d{4})-(\d{2})-(\d{2})|(\d{2})\/(\d{2})\/(\d{4}))$/" class="u-full-width" required/>		
		<br />
		<input type="submit" value="Save Book" ng-disabled="bookedit.$invalid" class="button u-pull-right" />
	</form>
</div>
<script type="text/javascript">

	jQuery('[name=publishedOn]').datetimepicker({
		timepicker: false,
		format: 'Y-m-d'
	});
	$('[name=tags]').tagsInput();

	var base_url='<?php echo base_url(); ?>';

	var app = angular.module('app',['ngMessages', 'ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		$scope.message="";
		$scope.bookId='<?php echo $this->uri->segment(3); ?>';
		var books=[];
		$http.get('<?php echo base_url(); ?>admin/get_books', {
			params: {bookId: $scope.bookId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.books = data;
			$scope.title=$scope.books[0].title;
			$scope.blurb=$scope.books[0].blurb;
			$scope.author=$scope.books[0].author;
			$scope.genre=$scope.books[0].genre;
			$scope.language=$scope.books[0].language;
			$scope.publisher=$scope.books[0].publisher;
			$scope.publishedOn=$scope.books[0].publishedOn;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.submit=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>admin/update_book/'+$scope.bookId,
					data: $.param({
						title: $scope.title,
						blurb: $scope.blurb,
						author: $scope.author,
						genre: $scope.genre,
						language: $scope.language,
						publisher: $scope.publisher,
						publishedOn: $scope.publishedOn
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data+"success");
					if(data != "success"){
						$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
					}else{
						$scope.message = "<div class='success'><i class='fa fa-check-circle'></i> Your changes have been saved!</div>";
						$scope.title="";
						$scope.blurb="";
						$scope.author="";
						$scope.genre="";
						$scope.language="";
						$scope.publisher="";
						$scope.publishedOn="";
						$scope.bookedit.$setPristine();
					}
				}).error(function(data){
					$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
				});
			}else{

				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, some fields in your form were invalid!</div>";
			}
		}
	}]);
</script>