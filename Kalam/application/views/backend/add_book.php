<h5>Add Book</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<?php if($note!="") echo $note; ?>
	<div ng-bind-html="message"></div>
	<?php echo form_open_multipart('admin/add_book', array('name'=>'bookadd','novalidate'=>'novalidate')) ?>
	<!-- <form name="bookadd" enctype="multipart/formdata" action="admin/add_book" novalidate> -->
	<!-- <form name="bookadd" enctype="multipart/formdata"  ng-submit="submit(bookadd.$valid)" novalidate> -->
	<label class="form-label">Title</label>
	<span ng-messages="bookadd.title.$dirty && bookadd.title.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the thread title.</span>
	</span>
	<input  type="text" name="title" class="u-full-width" ng-model="title" required />

	<label class="form-label">Cover</label>
	<span ng-messages="bookadd.cover.$dirty && bookadd.cover.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select a book cover.</span>
		<span class="invalid" ng-message="validFile"><i class="fa fa-close"></i> Please select a book cover.</span>
	</span>
	<input type="file" valid-file name="cover" class="u-full-width" ng-model="cover" accept="image/*" required />

	<label class="form-label">Blurb</label>
	<span ng-messages="bookadd.blurb.$dirty && bookadd.blurb.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the book blurb.</span>
		<span class="invalid" ng-message="minlength"><i class="fa fa-close"></i> The blurb must be atleast 10 characters long.</span>
	</span>
	<textarea name="blurb" class="u-full-width" ng-model="blurb" ng-minlength="10" required></textarea>

	<label class="form-label">Author</label>
	<span ng-messages="bookadd.author.$dirty && bookadd.author.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the author's name.</span>
	</span>
	<input type="text" name="author" class="u-full-width" ng-model="author" required />

	<label class="form-label">Genre</label>
	<span ng-messages="bookadd.genre.$dirty && bookadd.genre.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select a genre.</span>
	</span>
	<select class="u-full-width" name="genre" ng-model="genre" required>
		<?php 
		$this->load->helper('genre');
		$genres=get_genres();
		foreach ($genres as $value) {
			echo '<option value="'.$value.'">'.$value.'</option>';
		}
		?>			
	</select>

	<label class="form-label">Language</label>
	<span ng-messages="bookadd.language.$dirty && bookadd.language.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select the language.</span>
	</span>
	<select class="u-full-width" name="language" ng-model="language" required>
		<option value="Nepali">Nepali</option>
		<option value="English">English</option>
		<option value="Other">Other</option>
	</select>

	<label class="form-label">Publisher</label>
	<span ng-messages="bookadd.publisher.$dirty && bookadd.publisher.$error">
		<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the publisher's name.</span>
	</span>
	<input type="text" name="publisher" class="u-full-width" ng-model="publisher" required />

	<label class="form-label">Published Date: </label>
	<span ng-messages="signup.publishedOn.$dirty && signup.publishedOn.$error">
		<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your date of birth.</span>
		<span class="invalid" ng-message="pattern"> <i class="fa fa-close"></i> The date format must be YYYY-MM-DD.</span>
	</span>
	<input name="publishedOn" type="text"  ng-model="publishedOn" ng-pattern="/^((\d{4})-(\d{2})-(\d{2})|(\d{2})\/(\d{2})\/(\d{4}))$/" class="u-full-width" required/>


	<label>Tags</label>
	<input type="text" name="tags" ng-model="tags" />
	<br />
	<input type="submit" value="Add Book" ng-disabled="bookadd.$invalid || bookadd.$pending" class="button u-pull-right" />
</form>
</div>
<script type="text/javascript">

	jQuery('[name=publishedOn]').datetimepicker({
		timepicker: false,
		format: 'Y-m-d'
	});
	$('[name=tags]').tagsInput();

	var base_url='<?php echo base_url(); ?>';

	var app = angular.module('app',['ngMessages', 'ngSanitize']);
	
	app.directive('validFile',function(){
		return {
			require:'ngModel',
			link:function(scope,el,attrs,ngModel){
				el.bind('change',function(){
					scope.$apply(function(){
						ngModel.$setViewValue(el.val());
						ngModel.$render();
					});
				});
			}
		}
	});
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		$scope.message="";
		$scope.submit = function(isValid) {
			
		}
	}]);
</script>