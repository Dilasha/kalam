<div ng-app="app" ng-controller="ctrl">
	{{message}}
	<form name="test" ng-submit="submit()" enctype="multipart/form-data">
		<input accept="image/*" name="file" ng-value="fileToUpload" value="{{fileToUpload}}" file-model="fileToUpload" set-file-data="fileToUpload = value;" type="file"/>
		<input type="submit" value="Upload">
	</form>
</div>
<script>
	var base_url = "<?php echo base_url(); ?>";
	var app = angular.module('app',[]);
	app.directive("fileModel",function() {
		return {
			restrict: 'EA',
			scope: {
				setFileData: "&"
			},
			link: function(scope, ele, attrs) {
				ele.on('change', function() {
					scope.$apply(function() {
						var val = ele[0].files[0];
						scope.setFileData({ value: val });
					});
				});
			}
		}
	})
	app.service('fileUpload', ['$http', 'ajaxService',function($http, ajaxService) {
		this.uploadFileToUrl = function(data) {
			var data = {};
			var fd = new FormData();
			fd.append('file', data.file);

			$http.post("upload_file", fd, {
				withCredentials: false,
				headers: {
					'Content-Type': undefined
				},
				transformRequest: angular.identity,
				params: {
					fd
				},
				responseType: "arraybuffer"
			})
			.success(function(response, status, headers, config) {
				console.log(response);

				if (status == 200 || status == 202)
					console.log(response);
				else
					console.log(response);
			})
			.error(function(error, status, headers, config) {
				console.log(error);
			});
		}
	}]);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		$scope.message="not submitted";
		$scope.submit=function(fileUpload){
			$scope.message="submitted";
			// var file= $scope.
			// var uploadFileToUrl= "image_upload";
			// fileUpload.uploadFileToUrl(file); 
			// var data = new FormData();
/*			$http({
				method: 'post',
				url: base_url + 'admin/upload_file',
				data: {'text': $scope.text},
				headers: {'Content-Type': undefined}
			}).success(function(response){
				console.log(response+"success");
			}).error(function(response){
				console.log(response+"failure");
			});*/
		}
	}]);
</script>