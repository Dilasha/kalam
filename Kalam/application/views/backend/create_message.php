<h5>Create Message</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
<div ng-bind-html="message"></div>
	<form name="notify" ng-submit="send(notify.$valid)" novalidate="">
		<label class="form-label">Send To</label>   
		<span ng-messages="notify.reciever.$dirty && notify.reciever.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please select a reciever</span>
		</span>
		<select name="reciever" ng-model="reciever" required="" class="u-full-width">
			<option ng-repeat="user in users" value="{{user.userId}}">{{user.firstname}} {{user.lastname}}</option>
		</select>

		<label class="form-label">Message</label>
		<span ng-messages="notify.msg.$dirty && notify.msg.$error">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the message.</span>
		</span>
		<textarea class="u-full-width" name="msg" ng-model="msg" required></textarea>

		<br />
		<button ng-disabled="notify.$invalid" type="submit">Send</button>
	</form>
</div>
<script type="text/javascript">
	var app=angular.module('app',['ngMessages','ngSanitize']);
	app.controller('ctrl', ['$http', '$scope', function($http, $scope){
		var users=[];
		$http.get('<?php echo base_url(); ?>admin/get_users', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.users = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			users = []
			$http.get("<?php echo base_url(); ?>admin/get_users")
			.then(function(response) {
				console.log(response);
				users = response.data;
				console.log(users);
				$scope.users = users
			});
		}, 3000);*/
		$scope.send=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>admin/send_notification',
					data: $.param({
						'reciever': $scope.reciever,
						'message': $scope.msg
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data, status, headers, config) {
					console.log(data);
					if (data == "success") {
						$scope.message="<div class='success'><i class='fa fa-check-circle'></i>  Success, your message was sent!</div>";
						$scope.reciever = "";
						$scope.msg = "";
						$scope.notify.$setPristine();
					} else {
						$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
						$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					}
				}).error(function(response) {
					console.log(response);
					$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					swal('Oops!', 'There were some errors in your form', 'error')
				});
			}else{
				swal('Oops!', 'There were some errors in your form', 'error')
			}
		}
	}]);
</script>