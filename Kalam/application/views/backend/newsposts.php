<h5>View Newsposts</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="row search-sort">
		<div class="columns three">
			<label class="search-label">Search Key</label>
			<input type="text" class="u-full-width" name="keyword" data-ng-model="keyword"/>
		</div>
		<div class="columns three">
			<label class="search-label">Sort By</label>
			<select name="sort" class="u-full-width" ng-model="sort">
				<option value="">Default</option>
				<option value="heading">Heading</option>
				<option value="author">Author</option>
				<option value="publishedOn">Date Published</option>
			</select>
		</div>
		<div class="columns six">
			<br />
			<a href="<?php echo base_url(); ?>admin/newsposts" class="button u-pull-right"><i class="fa fa-plus"></i> Create New</a>
		</div>
	</div>
	<div class="row list-box"  id="newspost-{{newspost.newspostId}}" ng-repeat="newspost in newsposts| filter:keyword |orderBy:sort">
		<div class="row">
			<h6 class="columns eight">{{newspost.heading|uppercase}}</h6>
			<div class="columns four list-links right">
				<a href="<?php echo base_url(); ?>admin/edit_newspost/{{newspost.newspostId}}" class="button">Edit</a>
				<button class="button" ng-click="deleteNewspost(newspost)">Delete</button>
			</div>
		</div>
		<div class="row list-body" ng-bind-html="newspost.text | linebreak | to_trusted">
			{{newspost.text}}
		</div>	
		<div class="row">
			<strong>Published by:</strong>  {{newspost.author}} on  {{newspost.publishedOn}}
		</div>
	</div>
</div>
<script>
	var app=angular.module('app',[]);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var newsposts=[];
		$http.get('<?php echo base_url(); ?>admin/get_newsposts', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.newsposts = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			newsposts = []
			$http.get("<?php echo base_url(); ?>admin/get_newsposts")
			.then(function(response) {
				console.log(response);
				newsposts = response.data;
				console.log(newsposts);
				$scope.newsposts = newsposts
			});
		}, 3000);*/
		

		$scope.deleteNewspost = function(newspost) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_newspost',
				data: $.param({
					'newspostId': newspost.newspostId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#newspost-" + newspost.newspostId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>