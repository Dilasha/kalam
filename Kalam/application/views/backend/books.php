<h5>View Books</h5>
<div class="row" ng-app="app" ng-controller="ctrl">	
	<div class="row search-sort">
		<div class="columns three">
			<label class="search-label">Search Key</label>
			<input type="text" class="u-full-width" name="keyword" data-ng-model="keyword"/>
		</div>
		<div class="columns three">
			<label class="search-label">Sort By</label>
			<select name="sort" class="u-full-width" ng-model="sort">
				<option value="">Default</option>
				<option value="title">Title</option>
				<option value="author">Author Name</option>
				<option value="genre">Genre</option>
				<option value="language">Language</option>
				<option value="publisher">Publisher</option>
				<option value="publishedOn">Publish Date</option>
			</select>
		</div>
		<div class="columns six">
			<br/>
			<a href="<?php echo base_url(); ?>admin/add_book" class="button u-pull-right"><i class="fa fa-plus"></i> Add Book</a>
		</div>
	</div>

	<div class="row list-box" id="book-{{book.bookId}}" ng-repeat="book in books| filter:keyword |orderBy: sort:false">
		<div class="row">
			<h6 class="columns eight">{{book.title|uppercase}} by {{book.author}}</h6> 
			<div class="columns four right">
				<a href="<?php echo base_url(); ?>admin/edit_book/{{book.bookId}}" class="button">Edit</a>
				<button class="button button-default" ng-click="deleteBook(book)">Delete</button>
			</div>
		</div>
		<div class="row">			
			<div class="columns two">
				<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{book.cover}}" alt="{{book.title}}" />
			</div>
			<div class="columns ten">
				<label>Blurb</label>
				{{book.blurb|limitTo:600}}...
				<table class="u-full-width">
					<tbody>
						<tr>
							<td><strong>Language:</strong>  {{book.language}}</td>
							<td><strong>Publication:</strong>  {{book.publisher}}</td>
							<td><strong>Published date:</strong>  {{book.publishedOn}}</td>
						</tr>
						<tr>	
							<td><strong>Genre:</strong>  {{book.genre}}</td>
							<td colspan="2"><strong>Tags</strong>  {{book.tags}}</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>	
	</div>
</div>
<script>
	var app=angular.module('app',[]);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var books=[];
		$http.get('<?php echo base_url(); ?>admin/get_books', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.books = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			books = []
			$http.get("<?php echo base_url(); ?>admin/get_books")
			.then(function(response) {
				console.log(response);
				books = response.data;
				console.log(books);
				$scope.books = books
			});
		}, 3000);*/
		

		$scope.deleteUser = function(book) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_book',
				data: $.param({
					'bookId': book.bookId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#book-" + book.bookId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>