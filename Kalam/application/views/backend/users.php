<h5>View Users</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="row search-sort">
		<div class="columns three">
			<label class="search-label">Search Key</label>
			<input type="text" class="u-full-width" name="keyword" data-ng-model="keyword"/>
		</div>
		<div class="columns three">
			<label class="search-label">Sort By</label>
			<select name="sort" class="u-full-width" ng-model="sort">
				<option value="">Default</option>
				<option value="firstname">Firstname</option>
				<option value="lastname">Lastname</option>
				<option value="lastVisit">Last Online</option>
				<option value="registeredOn">Registerd Date</option>}
				<option value="role">Role</option>
			</select>
		</div>
		<div class="columns six">
			<br />
			<a href="<?php echo base_url(); ?>admin/users" class="button u-pull-right"><i class="fa fa-plus"></i> Create New</a>
		</div>
	</div>
	<table class="u-full-width">
		<thead>
			<th>Fullname</th>
			<th>Email</th>
			<th>Date of Birth</th>
			<th>Country</th>
			<th>Last Online</th>
			<th>Role</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tr  id="user-{{user.userId}}" ng-repeat="user in users | filter:keyword |orderBy:sort">
			<td>{{user.firstname}} {{user.lastname}}</td>
			<td>{{user.email}}</td>
			<td>{{user.dob}}</td>
			<td>{{user.country}}</td>
			<td>{{user.lastVisit}}</td>
			<td>{{user.role}}</td>
			<td><a href="<?php echo base_url(); ?>admin/edit_user/{{user.userId}}" class="button button-default"><i class="fa fa-edit"></i>  Edit</a></td>
			<td><a class="button button-default" ng-click="deleteUser(user)"><i class="fa fa-trash-o"></i> Delete</a></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	var app=angular.module('app',[]);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		var users=[];
		$http.get('<?php echo base_url(); ?>admin/get_users', {
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.users = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			users = []
			$http.get("<?php echo base_url(); ?>admin/get_users")
			.then(function(response) {
				console.log(response);
				users = response.data;
				console.log(users);
				$scope.users = users
			});
		}, 3000);*/

		$scope.deleteUser = function(user) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_user',
				data: $.param({
					'userId': user.userId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#user-" + user.userId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>