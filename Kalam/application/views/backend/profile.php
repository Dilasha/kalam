<h5>Profile</h5>
<?php if($note!=null) echo $note; ?>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="row" ng-repeat="user in users">
		<div class="columns three">
			<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{user.photo}}" alt="">
			<button class="u-full-width" ng-click="isReplyFormOpen = !isReplyFormOpen">
				<span ng-show="!isReplyFormOpen">Change Picture</span>
				<span ng-show="isReplyFormOpen">Cancel</span>
			</button>
			<?php echo form_open_multipart(base_url().'admin/change_photo', array('ng-show'=>'isReplyFormOpen')); ?>
			<input name="photo" class="u-full-width" type="file" required/>
			<input class="u-full-width" type="submit"  value="Change" />
			<?php echo form_close(); ?>
		</div>
		<div class="right"> 
			<a class="button" href="<?php echo base_url(); ?>admin/edit_user/<?php echo $user; ?>"><i class="fa fa-edit"></i> Edit</a>
			<a class="button" href="<?php echo base_url(); ?>admin/password_reset"><i class="fa fa-unlock-alt"></i> Password Reset</a>
		</div>
		<table class="u-full-width">
			<tr>
				<td><strong>Last Name:</strong></td>
				<td>{{user.lastname}}</td>
				<td><strong>First Name:</strong></td>
				<td>{{user.firstname}}</td>
			</tr>
			<tr>
				<td><strong>Date of Birth:</strong></td>
				<td>{{user.dob}}</td>
				<td><strong>Country:</strong></td>
				<td>{{user.country}}</td>
			</tr>
			<tr>
				<td><strong>Email Address:</strong></td>
				<td>{{user.email}}</td>
				<td><strong>Member Since:</strong></td>
				<td>{{user.registeredOn}}</td>
			</tr>
			<tr>
				<td><strong>Bio:</strong></td>
				<td colspan="3">{{user.bio}}</td>
			</tr>
		</table>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
	var userId='<?php echo $user; ?>';
	var app=angular.module('app',['ngMessages']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		var users=[];
		$http.get('<?php echo base_url(); ?>admin/get_users',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.users=data;
		}).error(function(data){
			console.log(data);
		});
	}]);
</script>