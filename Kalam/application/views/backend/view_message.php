<div class="row" ng-app="app" ng-controller="ctrl">
<div ng-bind-html="message"></div>
	<div id="message" ng-repeat="message in messages">
		<table class="u-max-full-width">
			<tr>
				<th>Sender</th>
				<td>{{message.sender}}</td>
			</tr>
			<tr>
				<th>Sender email</th>
				<td>{{message.email}}</td>
			</tr>
			<tr>
				<th>Subject</th>
				<td>{{message.subject}}</td>
			</tr>
			<tr>
				<th>Message</th>
				<td>{{message.message}}</td>
			</tr>
		</table>

		<a class="button" ng-click="deleteMessage(message)"><i class="fa fa-trash-o"></i> Delete</a>
	</div>
		<a class="button" href="<?php echo base_url(); ?>admin/messages"><i class="fa fa-chevron-left"></i>   Back</a>
	
</div>
<script type="text/javascript">
	var messageId='<?php echo $this->uri->segment(3); ?>';
	var app=angular.module('app',['ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', '$window', function($scope, $http, $window){
		var messages=[];
		$http.get('<?php echo base_url(); ?>admin/get_messages', {
			params: {'messageId': messageId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.messages = data;
			if($scope.messages[0].seen==0){
				$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/seen',
				data: $.param({
					'messageId': messageId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					console.log("marked as read");
				} else {
					console.log("unread")
				}
			}).error(function(data, status) {
				console.log("unread");			
			});
			}
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.deleteMessage = function(message) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>admin/delete_message',
				data: $.param({
					'messageId': message.messageId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$scope.message="<span class='failure'><i class='fa fa-check-circle'></i>  Successfully deleted!.</span>"
					$("#message").remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>