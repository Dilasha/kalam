<h5>Edit Profile</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="profileedit" ng-submit="update(profileedit.$valid)" novalidate="">
		<label class="form-label">Last Name: </label>
		<span ng-messages="profileedit.lastname.$dirty && profileedit.lastname.$error">
			<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your last name.</span>
		</span>
		<input name="lastname" type="text" ng-model="lastname" class="u-full-width" required/>

		<label class="form-label">First Name: </label>
		<span ng-messages="profileedit.firstname.$dirty && profileedit.firstname.$error">
			<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your first name.</span>
		</span>
		<input name="firstname" type="text" ng-model="firstname" class="u-full-width" required/>

		<label class="form-label">Date of Birth: </label>
		<span ng-messages="profileedit.dob.$dirty && profileedit.dob.$error">
			<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your date of birth.</span>
			<span class="invalid" ng-message="pattern"> <i class="fa fa-close"></i> The date format must be YYYY-MM-DD.</span>
		</span>
		<input name="dob" type="text" ng-model="dob" ng-pattern="/^((\d{4})-(\d{2})-(\d{2})|(\d{2})\/(\d{2})\/(\d{4}))$/" class="u-full-width" required/>

		<label class="form-label">Country: </label>
		<span ng-messages="profileedit.country.$dirty && profileedit.country.$error">
			<span class="invalid" ng-message="required"> <i class="fa fa-close"></i> Please enter your country of origin.</span>
		</span>
		<select class="u-full-width" name="country" ng-model="country" required>
			<?php 
			$this->load->helper('countries');
			$countries=get_countries();
			foreach ($countries as $value) {
				echo '<option value="'.$value.'">'.$value.'</option>';
			}
			?>			
		</select>

		<label class="form-label">Bio: </label>
		<span ng-messages="profileedit.bio.$dirty && profileedit.bio.$error">
			<span class="invalid" ng-message="maxlength"> <i class="fa fa-close"></i> Your bio cannot exceed 500 characters.</span>
		</span>
		<textarea name="bio" ng-model="bio" ng-maxlength="500" class="u-full-width"></textarea>

		<input class="u-pull-right" type="submit" ng-disabled="profileedit.$invalid" value="Save Changes" />
	</form>
</div>
<script type="text/javascript">
	jQuery('[name=dob]').datetimepicker({
		timepicker: false,
		format: 'Y-m-d'
	});
	var userId='<?php echo $user; ?>';
	var app=angular.module('app',['ngMessages','ngSanitize']);
	app.controller('ctrl', ['$http', '$scope', function($http, $scope){
		$scope.available=true;
		var users=[];
		$http.get('<?php echo base_url(); ?>users/get_users',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.users=data;
			$scope.lastname=$scope.users[0].lastname;
			$scope.firstname=$scope.users[0].firstname;
			$scope.country=$scope.users[0].country;
			$scope.dob=$scope.users[0].dob;
			$scope.bio=$scope.users[0].bio;
		}).error(function(data){
			console.log(data);
		});
		$scope.update=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>users/edit_profile',
					data: $.param({
						lastname: $scope.lastname,
						firstname: $scope.firstname,
						dob: $scope.dob,
						country: $scope.country,
						bio: $scope.bio
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data){
					console.log(data+"success");
					if(data != "success"){
						$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
					}else{
						$scope.message = "<div class='success'><i class='fa fa-check-circle'></i> Your changes have been saved!</div>";
						$scope.lastname="";
						$scope.firstname="";
						$scope.dob="";
						$scope.country="";
						$scope.bio="";
						$scope.available=true;
						$scope.profileedit.$setPristine();
					}
				}).error(function(data){
					$scope.message = "<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, your request could not be processed.</div>";
				});
			}else{

				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i> Oops, some field in your form were invalid!</div>";
			}
		}
	}]);
</script>