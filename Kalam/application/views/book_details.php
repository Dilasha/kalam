<div class="row" ng-app="app" ng-controller="ctrl">
	<div  ng-repeat="book in books|limitTo:1">
		<h5 class="row">{{book.title}} <span class="sub">by {{book.author}}</span></h5>
		<div class="row">
			<div class="columns three">
				<table class="u-full-width">
					<tr>
						<td><img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{book.cover}}" alt=""></td>
					</tr>	
				</table>
			</div>
			<div class="columns nine">
				<table class="u-full-width">
					<tr>
						<td colspan="3"><strong>Synopsis</strong></td>
					</tr>
					<tr>
						<td colspan="3">{{book.blurb}}</td>
					</tr>
					<tr>
						<td><strong>Author:</strong>  {{book.author}}</td>
						<td><strong>Genre:</strong>  {{book.genre}}</td>
						<td><strong>Language:</strong>  {{book.language}}</td>
					</tr>
					<tr>
					</tr>
					<tr>
						<td><strong>Publisher:</strong>  {{book.publisher}}</td>
						<td><strong>Published on:</strong>  {{book.publishedOn}}</td>
					</tr>
					<tr>
						<td colspan="3"><strong>Tags:</strong>  <span class="tags">{{book.tags}}</span></td>
					</tr>
					<tr>
						<td>
							<div ng-init="rating = star.rating + bookrating"></div>
							<!-- <div ng-init="rating = star.rating + 1"></div> -->
							<div class="star-rating" star-rating rating-value="rating" data-max="5" on-rating-selected="rateFunction(rating)"></div>
						</td>
					</tr>
				</table>
			</div>	
		</div>
	</div>
	<div class="row">
		<h6>Reviews</h6>
		<div class='row' id="rowc-{{review.reviewId}}" ng-repeat="review in reviews">
			<div class="columns one">
				<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{review.photo}}" alt="">
			</div>
			<div class="columns eight">
				<strong>{{review.firstname}} {{review.lastname}}</strong> says..
				<p  ng-bind-html="review.review | linebreak | to_trusted"></p>
			</div>
			<hr />
		</div>
		<br />
		<?php if ($this->session->userdata('sessiondata')!=null): ?>
			<div class="row">				
				<form name="reviewadd" ng-submit="review()" novalidate="">
					<text-angular placeholder="Add a review" class="u-full-width" name="text" ng-model="text" required /></text-angular>
					<br />
					<input type="submit" ng-disabled="reviewadd.$invalid" class="button u-pull-right" value="Post" />
				</form>
			</div>
		<?php endif ?>
	</div>
</div>
<script type="text/javascript">
	var app=angular.module('app',['textAngular' ,'ngSanitize']);
	app.directive('starRating',function() {
		return {
			restrict : 'A',
			template : '<ul class="rating">'
			+ '	<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">'
			+ '\u2605'
			+ '</li>'
			+ '</ul>',
			scope : {
				ratingValue : '=',
				max : '=',
				onRatingSelected : '&'
			},
			link : function(scope, elem, attrs) {
				var updateStars = function() {
					scope.stars = [];
					for ( var i = 0; i < scope.max; i++) {
						scope.stars.push({
							filled : i < scope.ratingValue
						});
					}
				};
				scope.toggle = function(index) {
					scope.ratingValue = index + 1;
					scope.onRatingSelected({
						rating : index + 1
					});
				};
				scope.$watch('ratingValue',function(oldVal, newVal) {
					if (newVal) {
						updateStars();
					}
				});
			}
		};
	});
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		$scope.bookId='<?php echo $this->uri->segment(3); ?>';
		var books=[];
		$http.get('<?php echo base_url(); ?>users/get_books', {
			params: {bookId: $scope.bookId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.books = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		var ratings=[];
		$http.get('<?php echo base_url(); ?>users/get_rating', {
			params: {bookId: $scope.bookId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.ratings = data;
			if($scope.ratings != 'false'){
				console.log('has data');
				$scope.r = $scope.ratings[0].rating;

			}else{
				console.log('is false');
				$scope.r = '0';
			}
			$scope.bookrating = $scope.r;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);

			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		var reviews=[];
		$http.get('<?php echo base_url(); ?>users/get_reviews', {
			params: {bookId: $scope.bookId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.reviews = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$scope.review=function(){
			console.log('done');
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/add_review',
				data: $.param({
					'bookId':$scope.bookId,
					'review':$scope.text
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data == "success") {
					$scope.text="";
					$scope.reviewadd.$setPristine();
				} else {
					swal('Oops!','There was an error processing your request.')
				}
			}).error(function(response) {
				console.log(response);
				swal('Oops!', 'There were some errors in your form', 'error')
			});
		}
		// $interval(function() {
		// 	reviews = [];
		// 	$http.get('<?php echo base_url(); ?>users/get_reviews', {
		// 		params: {bookId: $scope.bookId},
		// 		header: {
		// 			'Content-Type': 'application/json; charset=UTF-8'
		// 		}
		// 	}).
		// 	success(function(data) {
		// 		console.log(data);
		// 		$scope.reviews = data;
		// 	});
		// }, 2000);
		$scope.rating = 5;
		$scope.rateFunction = function(rating) {
			console.log('You selected - ' + rating+' stars');
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/add_rating',
				data: $.param({
					'rating': rating,
					'bookId': $scope.bookId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
			}).error(function(response) {
				console.log(response);
				swal('Oops!', 'your request could not be processed!', 'error')
			});
		};

	}]);	
	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>