<h5>Profile</h5>
<?php if($note!=null) echo $note; ?>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="columns eight">
		<div class="row" ng-repeat="user in users">
			<div class="columns three">
				<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{user.photo}}" alt="">
				<button class="u-full-width" ng-click="isReplyFormOpen = !isReplyFormOpen">
					<span ng-show="!isReplyFormOpen">Change Picture</span>
					<span ng-show="isReplyFormOpen">Cancel</span>
				</button>
				<?php echo form_open_multipart(base_url().'users/change_photo', array('ng-show'=>'isReplyFormOpen')); ?>
				<input name="photo" class="u-full-width" type="file" required/>
				<input class="u-full-width" type="submit"  value="Change" />
				<?php echo form_close(); ?>
			</div>
			<div class="columns eight offset-by-one">

				<table class="u-full-width">
					<div class="right"> 
						<a class="button button-default" href="<?php echo base_url(); ?>welcome/profile_edit"><i class="fa fa-edit"></i> Edit</a>
						<a class="button button-default" href="<?php echo base_url(); ?>welcome/password_reset"><i class="fa fa-unlock-alt"></i> Password Reset</a>
					</div>
					<tr>
						<td><strong>Last Name:</strong></td>
						<td>{{user.lastname}}</td>
						<td><strong>First Name:</strong></td>
						<td>{{user.firstname}}</td>
					</tr>
					<tr>
						<td><strong>Date of Birth:</strong></td>
						<td>{{user.dob}}</td>
						<td><strong>Country:</strong></td>
						<td>{{user.country}}</td>
					</tr>
					<tr>
						<td><strong>Email Address:</strong></td>
						<td>{{user.email}}</td>
						<td><strong>Member Since:</strong></td>
						<td>{{user.registeredOn}}</td>
					</tr>
					<tr>
						<td><strong>Bio:</strong></td>
						<td colspan="3">{{user.bio}}</td>
					</tr>
				</table>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="row">

				<div class="columns six">
					<h6>Reviews</h6>
					<p  ng-repeat="review in reviews">
						{{review.firstname}} reviewed <a href="<?php echo base_url(); ?>welcome/book_details/{{review.bookId}}"><strong>{{review.title}}</strong></a>
						on {{review.reviewedOn}}			
					</p>
				</div>
				<div class="columns six">
					<h6>Replies</h6>
					<p ng-repeat="reply in replies">
						{{reply.firstname}} replied to the thread <a href="<?php echo base_url(); ?>welcome/forum_thread/{{reply.threadId}}"><strong>{{reply.title}}</strong></a>
						on {{reply.repliedOn}}
					</p>				
				</div>
			</div>

		</div>
	</div>
	<div class="columns three offset-by-one sidebar">
		<h6>My Lists</h6>
		<ul>
			<li id="list-{{list.listId}}" ng-repeat="list in lists"><a href="<?php echo base_url(); ?>welcome/view_list/{{list.listId}}">{{list.listtitle}}</a> <a class="button-default" ng-click=deleteList(list)><i class="fa fa-trash-o"></i></a></li>
		</ul>
		<div ng-bind-html="message"></div>
		<form name="listadd" ng-submit="newList(listadd.$valid)" novalidate="">
			<span ng-messages="listadd.listtitle.$dirty && listadd.listtitle.$error">
				<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter the list name.</span>
			</span>
			<input type="text" class="u-full-width" name="listtitle" ng-model="listtitle" required />
			<input type="submit" class="u-pull-right" value="Add List" ng-disabled="newList.$invalid" />
		</form>
		<br />
		<h6>My Threads</h6>
		<ul>
			<li id="thread-{{thread.threadId}}" ng-repeat="thread in threads"><a href="<?php echo base_url(); ?>welcome/forum_thread/{{thread.threadId}}">{{thread.title}}</a> <a class="button-default" ng-click=deleteThread(thread)><i class="fa fa-trash-o"></i></a></li>
		</ul>
		<a href="<?php echo base_url(); ?>welcome/create_thread" class="button u-pull-right">Add Thread</a>

		
	</div>
</div>
<script type="text/javascript">
	var userId='<?php echo $user; ?>';
	var app=angular.module('app',['ngMessages']);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var users=[];
		$http.get('<?php echo base_url(); ?>users/get_users',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.users=data;
		}).error(function(data){
			console.log(data);
		});
		var reviews=[];
		$http.get('<?php echo base_url(); ?>users/get_reviews',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.reviews=data;
		}).error(function(data){
			console.log(data);
		});
		// $interval(function() {
		// 	reviews=[];
		// 	$http.get('<?php echo base_url(); ?>users/get_reviews',{
		// 		params:{'userId': userId},
		// 		header: {'Content-Type': 'application/json; charset=UTF-8'}
		// 	}).success(function(data){
		// 		console.log(data);
		// 		$scope.reviews=data;
		// 	}).error(function(data){
		// 		console.log(data);
		// 	});
		// }, 2000);
		var replies=[];
		$http.get('<?php echo base_url(); ?>users/get_replies',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.replies=data;
		}).error(function(data){
			console.log(data);
		});

		// 	$interval(function() {
		// 		replies=[];
		// 		$http.get('<?php echo base_url(); ?>users/get_replies',{
		// 			params:{'userId': userId},
		// 			header: {'Content-Type': 'application/json; charset=UTF-8'}
		// 		}).success(function(data){
		// 			console.log(data);
		// 			$scope.replies=data;
		// 		}).error(function(data){
		// 			console.log(data);
		// 		});
		// 	}, 2000);

		var threads=[];
		$http.get('<?php echo base_url(); ?>users/get_threads',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.threads=data;
		}).error(function(data){
			console.log(data);
		});

		var lists=[];
		$http.get('<?php echo base_url(); ?>users/get_lists',{
			params:{'userId': userId},
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.lists=data;
		}).error(function(data){
			console.log(data);
		});
		$scope.newList=function(isValid){
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/add_list',
				data: $.param({
					'listtitle': $scope.listtitle,
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data == "success") {
					$scope.listtitle = "";
					$scope.listadd.$setPristine();
					lists=[];
					$http.get('<?php echo base_url(); ?>users/get_lists',{
						params:{'userId': userId},
						header: {'Content-Type': 'application/json; charset=UTF-8'}
					}).success(function(data){
						console.log(data);
						$scope.lists=data;
					}).error(function(data){
						console.log(data);
					});
				} else {
					$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
				}
			}).error(function(response) {
				console.log(response);
				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
			});				
		}
		$scope.deleteList = function(list) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/delete_list',
				data: $.param({
					'listId': list.listId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#list-" + list.listId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}

		$scope.deleteThread = function(thread) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/delete_thread',
				data: $.param({
					'threadId': thread.threadId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#thread-" + thread.threadId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);

app.filter('linebreak', function() {
	return function(text) {
		return text.replace(/\n/g, '<br>');
	}
}).filter('to_trusted', ['$sce', function($sce) {
	return function(text) {
		return $sce.trustAsHtml(text);
	};
}]);
</script>