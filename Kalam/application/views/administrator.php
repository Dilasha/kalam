<?php 
if($this->session->userdata('sessiondata')!=null){
    echo "<script>window.location='".base_url()."welcome'</script>";
}
?>
<div id="login" class="row" ng-app="app" ng-controller="ctrl">
    <h5>Adminintrator Login</h5>
    <div ng-bind-html="message"></div>

    <form name="login" ng-submit="submit(login.$valid)" novalidate="">
        <label class="form-label">Email Address</label>
        <span ng-messages="login.email.$dirty && login.email.$error">
            <span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter your email adress.</span>
            <span class="invalid" ng-message="email"><i class="fa fa-close"></i> The email address you entered is invalid.</span>
        </span>
        <input name="email" type="email" class="u-full-width" name="email" ng-model="email" required="" />

        <label class="form-label">Password</label>
        <span ng-messages="login.password.$dirty && login.password.$error">
            <span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please enter your email adress.</span>
        </span>
        <input name="password" type="password" class="u-full-width" name="password" ng-model="password"  required="" />

        <input type="submit" class="u-pull-right button-primary" value="Sign In" ng-disabled="login.$invalid" />
    </form>
</div>

<script type="text/javascript">
    var app=angular.module('app', ['ngMessages', 'ngSanitize']);
    app.controller('ctrl', ['$http', '$scope', '$window', function($http, $scope, $window){
        $scope.submit=function(isValid){
            if(isValid){
                $http({
                    method: 'post',
                    url: '<?php echo base_url(); ?>users/login',
                    data: $.param({
                        email: $scope.email,
                        password: $scope.password,
                        role:0
                    }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    console.log(data + "success");
                    if(data!="success"){
                        $scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your login credentials were incorrect!</div>"
                    }else{
                        $window.location = '<?php echo base_url(); ?>admin/dashboard';
                    }
                }).error(function(data){
                    $scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your login credentials were incorrect!</div>"
                });
            }else{
                $scope.message="error";
                swal('Oops!', 'There were some errors in your form', 'error');
            }
        }
    }]);
</script>