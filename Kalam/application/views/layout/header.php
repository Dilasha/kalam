<header>
	<ul class="right u-full-width">
		<li><a href="<?php echo base_url(); ?>welcome/about">About</a></li>
		<li><a href="<?php echo base_url(); ?>welcome/contact">Contact</a></li>
		
		<?php if ($this->session->userdata('sessiondata')!=null): ?>
			<?php $session= $this->session->userdata('sessiondata'); ?>		

			<li><a href="<?php echo base_url(); ?>welcome/profile"><?php echo $session['username']; ?>'s Profile</i></a></li>
			<li><a href="<?php echo base_url(); ?>welcome/notifications">Notifications ( <?php echo $ncount; ?> new )</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/logout">Logout</a></li>
		<?php else: ?>

			<li><a href="<?php echo base_url(); ?>welcome/register">Register</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/login">Login</a></li>

		<?php endif ?>
	</ul>	
</header>