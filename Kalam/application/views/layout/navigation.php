<nav>
	<div class="row">
		<span class="columns three">
			<a href="<?php echo base_url(); ?>welcome/"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="logo" class="logo"></a>
		</span>
		<ul class="ul nine">
			<li><a href="<?php echo base_url(); ?>welcome/index">Home</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/picks">Top Picks</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/catalogue">Book Catalogue</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/newsposts">News &amp; Events</a></li>
			<li><a href="<?php echo base_url(); ?>welcome/forums">Forums</a></li>
		</ul>

	</div>
</nav>