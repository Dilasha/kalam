<?php 
if($this->session->userdata('sessiondata')!=null){
	$session= $this->session->userdata('sessiondata');
	if($session['role']!=0){
		echo "<script>window.location='".base_url()."'</script>";
	}
}else{
	echo "<script>window.location='".base_url()."welcome'</script>";
}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin | Kalam</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/skeleton.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.tag-editor.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/angular-xeditable/0.1.12/css/xeditable.min.css">
	
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Hind:400,600,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/general.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/admin-style.css"> 

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.full.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.tagsinput.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>


	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-messages.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
	<script src='http://textangular.com/dist/textAngular-rangy.min.js'></script>
	<script src='http://textangular.com/dist/textAngular-sanitize.min.js'></script>
	<script src='http://textangular.com/dist/textAngular.min.js'></script>
</head>
<body>
	<?php $this->load->view('layout/admin-header') ?>
	<div class="container">
		<div class="row">
			<div class="columns two">
				<?php $this->load->view('layout/admin-navigation') ?>
			</div>
			<div class="columns offset-by-one eight content">
				<?php $this->load->view('backend/'.$content) ?>			
			</div>
		</div>
	</div>
	<?php $this->load->view('layout/admin-footer') ?>
</body>
</html>