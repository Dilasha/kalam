<nav>
	<ul>
		<a href="<?php echo base_url(); ?>admin/"><img class="logo" src="<?php echo base_url(); ?>assets/images/logo.png" alt="Kalam" /></a>
		<li><a href="<?php echo base_url(); ?>admin/">Dashboard</a></li>
		<li><a href="<?php echo base_url(); ?>admin/users">Add Users</a></li>
		<li><a href="<?php echo base_url(); ?>admin/view_users">View Users</a></li>
		<li><a href="<?php echo base_url(); ?>admin/books">View Books</a></li>
		<li><a href="<?php echo base_url(); ?>admin/add_book">Add Book</a></li>
		<li><a href="<?php echo base_url(); ?>admin/view_threads">View Threads</a></li>
		<li><a href="<?php echo base_url(); ?>admin/threads"> Add Thread</a></li>
		<li><a href="<?php echo base_url(); ?>admin/view_newsposts">View Newsposts</a></li>
		<li><a href="<?php echo base_url(); ?>admin/newsposts"> Create Newsposts</a></li>
		
	</ul>
</nav>