<h5>Password Reset</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div ng-bind-html="message"></div>
	<form name="passwordreset" ng-submit="change(passwordreset.$valid)" novalidate="">
		<label class="form-label">Current Password:</label>
		<span ng-messages="passwordreset.currentp.$dirty && passwordreset.currentp.$error ">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please supply your current password</span>
			<span class="invalid" ng-message="isCorrect"><i class="fa fa-close"></i> Your password is incorrect</span>
		</span>
		<input type="password" class="u-full-width" name="currentp" is-correct ng-model="currentp" required="" />

		<label class="form-label">New Password:</label>
		<span ng-messages="passwordreset.newp.$dirty && passwordreset.newp.$error ">
			<span class="invalid" ng-message="required"><i class="fa fa-close"></i> Please supply your new password</span>
			<span class="invalid" ng-message="minlength"><i class="fa fa-close"></i> Your new password must be at least 5 characters long.</span>
		</span>
		<input type="password" class="u-full-width" name="newp" ng-minlength="5" required ng-model="newp" />

		<label class="form-label">Confirm New Password:</label>
		<span ng-messages="passwordreset.newpc.$dirty && passwordreset.newpc.$error ">
			<span class="invalid" ng-message="compareTo"><i class="fa fa-close"></i> Password mismatch</span>
		</span>
		<input type="password"  compare-to="newp" class="u-full-width" name="newpc" ng-model="newpc" />

		<input type="submit" class="u-pull-right" ng-disabled="passwordreset.$invalid" value="Reset Password" />
	</form>
</div>
<script type="text/javascript">
	var user='<?php echo $user; ?>';
	var app=angular.module('app',['ngMessages', 'ngSanitize']);
	app.directive('compareTo',function() {
		return {
			require: "ngModel",
			scope: {
				passwordvalue: "=compareTo"
			},
			link: function(scope, element, attributes, ngModel) {
				ngModel.$validators.compareTo = function(modelValue) {
					return modelValue == scope.passwordvalue;
				};
				scope.$watch("passwordvalue", function() {
					ngModel.$validate();
				});
			}
		};
	});
	app.directive('isCorrect', ['$http', function($http){
		return {
			require: 'ngModel',
			link: function (scope, elm, attrs, ctrl){
				elm.bind('focusout', function (blurEvent) {
					$http({
						method: 'POST', 
						url: '<?php echo base_url(); ?>users/isCorrect', 
						data: $.param({password: scope.currentp}),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					}).then(function(result) {
						console.log(result.data);
						if(result.data == "correct"){
							scope.available=false;
							ctrl.$setValidity('isCorrect', true);
						}
						else{
							ctrl.$setValidity('isCorrect', false);
							scope.available=true;
						}
					});
				});
			}
		};
	}]);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		$scope.correct=true;
		$scope.change=function(isValid){
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/reset_password',
				data: $.param({
					'cpassword': $scope.currentp,
					'npassword': $scope.newp
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$scope.message="<div class='success'><i class='fa fa-check-circle'></i>  Success, your password was reset!</div>";
					$scope.currentp = "";
					$scope.newp="";
					$scope.newpc = "";
					$scope.passwordreset.$setPristine();
				} else {
					$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your current password was incorrect!</div>";
				}
			}).error(function(response) {
				console.log(response);
				$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
				swal('Oops!', 'There were some errors in your form', 'error')
			});
		}
	}]);
</script>