<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="columns ten">
		<div  ng-repeat="newspost in newsposts|limitTo:1">
			<h5 class="row">{{newspost.heading}}</h5>
			<div id="rown-{{newspost.newspostId}}" ng-repeat="newspost in newsposts">
				<div class="row body" ng-bind-html="newspost.text | linebreak | to_trusted">
					{{newspost.text}}
				</div>
				<hr />			
				<div class="row">
					<strong>Author:</strong> {{newspost.firstname}} {{newspost.lastname}}
					<br />
					<strong>Published Date:</strong> {{newspost.publishedOn}}
				</div>
			</div>
		</div>

		<hr />
		<div class="row">
			<h6>Comments</h6>
			<div class='row' id="rowc-{{comment.commentId}}" ng-repeat="comment in comments">
				<div class="columns one">
					<img class="u-max-full-width" src="<?php echo base_url(); ?>uploads/{{comment.photo}}" alt="">
				</div>
				<div class="columns eight">
					<strong>{{comment.firstname}} {{comment.lastname}}</strong> says..
					<p>
						{{comment.comment}}
					</p>

				</div>
			</div>
			<?php if ($this->session->userdata('sessiondata')!=null): ?>
				<div class="row">
					<div class="columns eight">
						<form name="commentadd" ng-submit="comment(commentadd.$valid)" novalidate="">
							<textarea name="text" ng-model="text" class="u-full-width" placeholder="Add a comment" required=""></textarea>
							<input type="submit" ng-disabled="commentadd.$invalid" class="button u-pull-right" value="Post" />
						</form>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	var app=angular.module('app',['ngSanitize']);
	app.controller('ctrl', ['$scope', '$http','$interval', function($scope, $http,$interval){
		$scope.newspostId='<?php echo $this->uri->segment(3); ?>';
		var newsposts=[];
		$http.get('<?php echo base_url(); ?>users/get_newsposts', {
			params: {newspostId: $scope.newspostId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.newsposts = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		var comments=[];
		$http.get('<?php echo base_url(); ?>users/get_comments', {
			params: {newspostId: $scope.newspostId},
			header: {
				'Content-Type': 'application/json; charset=UTF-8'
			}
		}).
		success(function(data) {
			console.log(data);
			$scope.comments = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});
		$interval(function() {
			comments=[];
			$http.get('<?php echo base_url(); ?>users/get_comments', {
				params: {newspostId: $scope.newspostId},
				header: {
					'Content-Type': 'application/json; charset=UTF-8'
				}
			}).
			success(function(data) {
				console.log(data);
				$scope.comments = data;
			}).
			error(function(data) {
				console.log("error");
				console.log(data);
				console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
			});
		}, 3000);
		$scope.comment=function(){
			console.log('done');
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/add_comment',
				data: $.param({
					'newspostId':$scope.newspostId,
					'comment':$scope.text,
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data == "success") {
					$scope.text="";
					$scope.commentadd.$setPristine();
				} else {
					swal('Oops!','There was an error processing your request.')
				}
			}).error(function(response) {
				console.log(response);
				swal('Oops!', 'There were some errors in your form', 'error')
			});
		}

	}]);
	app.filter('linebreak', function() {
		return function(text) {
			return text.replace(/\n/g, '<br>');
		}
	}).filter('to_trusted', ['$sce', function($sce) {
		return function(text) {
			return $sce.trustAsHtml(text);
		};
	}]);
</script>