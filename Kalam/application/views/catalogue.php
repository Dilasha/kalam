<h5>Books</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class=" columns eight row">
		<div class="row">
			<h6 align="center">Looking for something specific?</h6>
			<div class="row adv-search">
				<div class="columns three">
					<label>Sort By</label>
					<select class="u-full-width" name="sort" ng-model="sort">
						<option value="">Default</option>
						<option value="title">Title</option>
						<option value="author">Author Name</option>
						<option value="genre">Genre</option>
						<option value="language">Language</option>
						<option value="publisher">Publisher</option>
						<option value="publishedOn">Publish Date</option>
					</select>	
				</div>
				<div class="columns three">
					<label>Keyword</label>
					<input class="u-full-width" type="text" name="keyword" ng-model="keyword" placeholder="Enter Keyword" />
				</div>
				<div class="columns three">
					<label>Genre</label>
					<select class="u-full-width" name="genre" ng-model="genre" required>
						<option value="">Default</option>
						<?php 
						$this->load->helper('genre');
						$genres=get_genres();
						foreach ($genres as $value) {
							echo '<option value="'.$value.'">'.$value.'</option>';
						}
						?>			
					</select>
				</div>
				<div class="columns three">
					<label>Language</label>
					<label class="form-label">
						<input type="radio" name="slanguage" value="Nepali" ng-model="slanguage">
						<span class="label-body">Nepali</span>
					</label>
					<label class="form-label">
						<input type="radio" name="slanguage" value="English" ng-model="slanguage">
						<span class="label-body">English</span>
					</label>
				</div>
			</div>
		</div>
		<div class="columns three row book-box" id="rowb-{{book.bookId}}" ng-repeat="book in books| filter:keyword|filter:slanguage| filter:genre |orderBy:sort">
			<span class="book-holder u-full-width" style="background-image: url('<?php echo base_url(); ?>uploads/{{book.cover}}');"></span>
			<span><strong>{{book.title}}</strong></span>
			<span> by {{book.author}}</span>
			<span>
				<div ng-init="rating = star.rating + 1"></div>
				<div star-rating rating-value="rating" data-max="5"></div>
			</span>
			<?php if ($this->session->userdata('sessiondata')!=null): ?>
				<span><button ng-click="listform(book)" class="u-full-width button-default"><i class="fa fa-plus"></i> Add To List</button></span>
			<?php endif ?>
			<span><a href="<?php echo base_url(); ?>welcome/book_details/{{book.bookId}}" class="button button-default u-full-width"><i class="fa fa-list"></i> View Details</a></span>
		</div>
	</div>
	<?php if ($this->session->userdata('sessiondata')!=""): ?>
		<div class="columns three offset-by-one sidebar">
			<h6>My Lists</h6>
			<ul>
				<li ng-repeat="list in lists"><a href="<?php echo base_url(); ?>welcome/view_list/{{list.listId}}">{{list.listtitle}}</a></li>
			</ul>
		</div>
	<?php endif ?>
</div>
<script>
	var app= angular.module('app', []);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var books=[];
		$http.get('<?php echo base_url(); ?>users/get_books', {
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.books = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		/*$interval(function() {
			books = []
			$http.get("<?php echo base_url(); ?>users/get_books")
			.then(function(response) {
				console.log(response);
				books = response.data;
				console.log(books);
				$scope.books = books
			});
		}, 3000);*/

		var lists=[];
		$http.get('<?php echo base_url(); ?>users/get_lists', {
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).
		success(function(data) {
			console.log(data);
			$scope.lists = data;
		}).
		error(function(data) {
			console.log("error");
			console.log(data);
			console.log("AJAX error in request: " + JSON.stringify(data, null, 2));
		});

		$scope.listform=function(book){
			console.log(book.bookId);
			var listopt = [];
			for(var i = 0;i<$scope.lists.length;i++)
				listopt.push({value:$scope.lists[i].listId, text:$scope.lists[i].listtitle});

			swal.withForm({
				title: 'Select List',
				showCancelButton: true,
				allowOutsideClick: true,
				confirmButtonColor: '#265166',
				confirmButtonText: 'Add to list!',
				closeOnConfirm: true,
				formFields: [{ id: 'select', type: 'select', options: listopt}]
			}, function (isConfirm) {
				if(isConfirm){
					$scope.listId=this.swalForm.select;
					$http({
						method: 'post',
						url: '<?php echo base_url(); ?>users/add_to_list',
						data: $.param({
							'bookId': book.bookId,
							'listId': $scope.listId
						}),
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
					}).success(function(data, status, headers, config) {
						console.log(data);
						if (data == "success") {
							swal('Success','Added to list');
						} else {
							swal('Failure','There was an error :(');
						}
					}).error(function(response) {
						console.log(response);
					});
				}else{					
					swal('Failure','There was an error :(');
				}
			});
		}
		
	}]);
</script>	