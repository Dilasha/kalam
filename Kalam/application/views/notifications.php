<h5>Notifications</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<table class="u-full-width">
		<thead>
			<th>Notification</th>
			<th>Recieved on</th>
		</thead>
		<tr id="notification-{{notification.notificationId}}" ng-repeat="notification in notifications track by $index">
			<td>{{notification.message}}</td>
			<td><em>{{notification.sentOn}}</em>  <a ng-click="deleteNotification(notification)" class="button button-default"><i class="fa fa-trash-o"></i></a></td>
		</tr>
	</table>
</div>
<script type="text/javascript">
	var userId='<?php echo $user; ?>';
	var app=angular.module('app',['ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', '$interval', function($scope, $http, $interval){
		var notifications=[];
		$http.get('<?php echo base_url(); ?>users/get_notifications',{
			header: {'Content-Type': 'application/json; charset=UTF-8'}
		}).success(function(data){
			console.log(data);
			$scope.notifications=data;
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/notif_seen',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
			}).error(function(response) {
				console.log(response);
			});
		}).error(function(data){
			console.log(data);
		});
		$interval(function() {
			notifications=[];
			$http.get('<?php echo base_url(); ?>users/get_notifications',{
				params:{'reciever': userId},
				header: {'Content-Type': 'application/json; charset=UTF-8'}
			}).success(function(data){
				console.log(data);
				$scope.notifications=data;
			}).error(function(data){
				console.log(data);
			});
		}, 2000);
		$scope.deleteNotification = function(notification) {
			$http({
				method: 'post',
				url: '<?php echo base_url(); ?>users/delete_notification',
				data: $.param({
					'notificationId': notification.notificationId
				}),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data, status, headers, config) {
				console.log(data);
				if (data.trim() == "success") {
					$("#notification-" + notification.notificationId).remove();
				} else {
					swal("Oops!","Error deleting record","error");
				}
			}).error(function(data, status) {
				swal("Oops!","Error deleting record","error");				
			});
		}
	}]);
</script>