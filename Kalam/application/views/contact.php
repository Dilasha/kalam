<h5>Contact</h5>
<div class="row" ng-app="app" ng-controller="ctrl">
	<div class="columns eight">
		<p>
			Got any queries, complaints requests or suggestions?
			Fill the form below and let us know today! 
		</p>
		<div ng-bind-html="message"></div>
		<form name="emailsend" ng-submit="send(emailsend.$valid)" novalidate="">
			<label class="form-label">Your Name</label>
			<input type="text" name="sender" ng-model="sender" class="u-full-width" required/>

			<label class="form-label">Subject</label>
			<input type="text" name="subject" ng-model="subject" class="u-full-width" required/>

			<label class="form-label">Your Email</label>
			<input type="email" name="email" ng-model="email" class="u-full-width" required/>

			<label class="form-label">Message</label>
			<textarea name="msg" ng-model="msg" class="u-full-width"></textarea>
			<input type="submit" value="Send" ng-disabled="emailsend.$invalid" />
		</form>
	</div>
</div>
<script type="text/javascript">
	var app=angular.module('app',['ngMessages','ngSanitize']);
	app.controller('ctrl', ['$scope', '$http', function($scope, $http){
		$scope.send=function(isValid){
			if(isValid){
				$http({
					method: 'post',
					url: '<?php echo base_url(); ?>users/send_mail',
					data: $.param({
						'sender': $scope.sender,
						'subject': $scope.subject,
						'email': $scope.email,
						'msg': $scope.msg
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}).success(function(data, status, headers, config) {
					console.log(data);
					if (data == "success") {
						$scope.message="<div class='success'><i class='fa fa-check-circle'></i>  Success, your message was sent!</div>";
						$scope.sender = "";
						$scope.email="";
						$scope.subject="";
						$scope.msg = "";
						$scope.emailsend.$setPristine();
					} else {
						$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					}
				}).error(function(response) {
					console.log(response);
					$scope.message="<div class='failure'><i class='fa fa-exclamation-triangle'></i>  Oops, your request could not be processed!</div>";
					swal('Oops!', 'There were some errors in your form', 'error')
				});
			}else{
				swal('Oops!', 'There were some errors in your form', 'error')
			}
		}
	}]);
</script>