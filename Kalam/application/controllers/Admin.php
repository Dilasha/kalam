<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('generic');		
	}
	function index()
	{
		$data['content']='dashboard';
		$this->load->view('layout/backend', $data);
	}
	function dashboard()
	{
		$data['content']='dashboard';
		$this->load->view('layout/backend', $data);
	}
	function books()
	{
		$data['content']='books';
		$this->load->view('layout/backend', $data);	
	}
	function profile()
	{		
		$data['note']='';
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$data['content']='profile';
			$this->load->view('layout/backend', $data);	
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function send_notification(){
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data=array(
				'reciever'=> $this->security->xss_clean($this->input->post('reciever')),
				'message'=> $this->security->xss_clean($this->input->post('message')),
				'sentOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('notifications',$data))
				echo "success";
			else
				echo "failure";
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function create_message(){
		$data['note']='';
		$data['content']='create_message';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$data['username']=$session['username'];
		}else{
			redirect('welcome/login','refresh');
		}
		$this->load->view('layout/backend', $data);		
	}
	function password_reset()
	{	
		$data['note']='';
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$data['content']='password_reset';
			$this->load->view('layout/backend', $data);	
		}else{
			redirect('welcome/login','refresh');
		}
		
	}
	function view_users()
	{
		$data['content']='users';
		$this->load->view('layout/backend', $data);	
	}
	function edit_user(){
		$data['content']='edit_user';
		$this->load->view('layout/backend', $data);
	}
	function messages(){
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			if($session['role']!=0){
				redirect('welcome/index');
			}else{
				$data['user']=$session['uid'];
				$data['content']='messages';
				$this->load->view('layout/backend', $data);	
			}
		}else{
			redirect('welcome/login','refresh');
		}
	}

	function get_unread(){
		if($unread = $this->generic->record_select('messages','seen = 0','10000')){
			echo json_encode($unread);
		}else{
			echo "no records";
		}
	}

	function view_message(){
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			if($session['role']!=0){
				redirect('welcome/index');
			}else{
				$data['user']=$session['uid'];
				$data['content']='view_message';
				$this->load->view('layout/backend', $data);	
			}
		}else{
			redirect('welcome/login','refresh');
		}	
	}
	function get_messages(){
		if($this->input->get('messageId')!=null){
			$messages=$this->generic->record_select('messages','messageId ='.$this->input->get('messageId').' and seen = 0',1);
			echo json_encode($messages);			
		}else{
			$messages=$this->generic->record_list('messages');
			echo json_encode($messages);				
		}	
	}
	function seen(){
		if($this->input->post('messageId')!=null){
			$data=array(
				'seen'=>1
				);
			if($this->generic->record_update('messages', 'messageId = '.$this->input->post('messageId'), $data)){
				echo "success";
			}else{
				echo "failure";
			}
		}
	}
	function update_user(){
		if($this->input->post('userId')!=null)
		{
			$data = array(
				'lastname' => $this->security->xss_clean($this->input->post('lastname')),
				'firstname' => $this->security->xss_clean($this->input->post('firstname')),
				'dob' => $this->security->xss_clean($this->input->post('dob')),
				'country' => $this->security->xss_clean($this->input->post('country')),
				'bio' => $this->security->xss_clean($this->input->post('bio')),
				);
			if($this->generic->record_update('users', 'userId ='.$this->input->post('userId') ,$data))
				echo "success";
			else
				echo "failure";
		}else{
			echo "failure";
		}
	}
	function get_users()
	{		
		if($this->input->get('userId')!=null){
			$condition=array(
				'userId'=> $this->input->get('userId')
				);
			$users=$this->generic->record_select('users', $condition, 1);
			echo json_encode($users);
		}else{	
			$users=$this->generic->record_list('users');
			echo json_encode($users);
		}
	}
	function users(){
		$data['note']="";
		$data['content']='add_user';
		$this->load->view('layout/backend', $data);	
	}
	function add_user()
	{		
		$data = array(
			'lastname' => $this->security->xss_clean($this->input->post('lastname')),
			'firstname' => $this->security->xss_clean($this->input->post('firstname')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'password' => sha1($this->security->xss_clean($this->input->post('password'))),
			'dob' => $this->security->xss_clean($this->input->post('dob')),
			'photo'=> 'default.png',
			'country' => $this->security->xss_clean($this->input->post('country')),
			'role'=> $this->security->xss_clean($this->input->post('role')),
			'bio'=> $this->security->xss_clean($this->input->post('firstname')).' '.$this->security->xss_clean($this->input->post('lastname')).'was added by an Admin on'.unix_to_human(time(), TRUE, 'eu'),
			'registeredOn' => unix_to_human(time(), TRUE, 'eu')
			);
		if($this->generic->record_add('users', $data))
			echo "success";
		else
			echo "failure";
	}
	function get_books()
	{		
		if($this->input->get('bookId')!=null){
			$condition=array(
				'bookId'=> $this->input->get('bookId')
				);
			$books=$this->generic->record_select('books', $condition, 1);
			echo json_encode($books);
		}else{	
			$books=$this->generic->record_list('books');
			echo json_encode($books);
		}
	}
	function add_book()
	{
		$data['note']="";
		$this->form_validation->set_error_delimiters('<span class="invalid">', '</span>');
		$this->form_validation->set_rules('title', 'Title', 'required');
		if ($this->form_validation->run() == FALSE) {
		}else{			
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['max_size']             = 2000;
			$config['max_width']            = 2000;
			$config['max_height']           = 2000;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('cover'))
			{
				$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>" .$this->upload->display_errors('<span>','</span>')."</div>";
			}
			else
			{
				$file = array('upload_data' => $this->upload->data());
				$filename=$file['upload_data']['file_name'];
				$data=array(
					'title' => $this->input->post('title'),
					'cover' => $filename,
					'blurb' => $this->input->post('blurb'),
					'author' => $this->input->post('author'),
					'genre' => $this->input->post('genre'),
					'language' => $this->input->post('language'),
					'tags'  => $this->input->post('tags'),
					'publisher'  => $this->input->post('publisher'),
					'publishedOn'  => $this->input->post('publishedOn')
					);
				if($this->generic->record_add('books',$data))
					$data['note']="<div class='success'><i class='fa fa-check-circle'></i>Success! Work Added.</div>";
				else
					$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>Oops! Your request could not be processed at the moment.</div>";
			}			
		}	
		$data['content']='add_book';
		$this->load->view('layout/backend', $data);
	}
	function delete_book(){
		$condition=array(
			'bookId' => $this->input->post('bookId')
			);
		if($this->generic->record_delete('books', $condition))
			echo "success";
		else
			echo "failure";
	}
	function delete_message(){
		$condition=array(
			'messageId' => $this->input->post('messageId')
			);
		if($this->generic->record_delete('messages', $condition))
			echo "success";
		else
			echo "failure";
	}
	function update_book(){
		$bookId=$this->uri->segment(3);
		$data=array(
			'title'=> $this->security->xss_clean($this->input->post('title')),
			'blurb'=> $this->security->xss_clean($this->input->post('blurb')),
			'author'=> $this->security->xss_clean($this->input->post('author')),
			'genre'=> $this->security->xss_clean($this->input->post('genre')),
			'language'=> $this->security->xss_clean($this->input->post('language')),
			'publisher'=> $this->security->xss_clean($this->input->post('publisher')),
			'publishedOn'=> $this->security->xss_clean($this->input->post('publishedOn')),
			);
		if($this->generic->record_update('books','bookId ='.$bookId,$data))
			echo "success";
		else
			echo "failure";
	}
	function edit_book(){
		$data['note']='';
		$data['content']='edit_book';
		$this->load->view('layout/backend', $data);
	}
	function edit_newspost(){
		$data['content']='edit_newspost';
		$this->load->view('layout/backend', $data);
	}
	function edit_thread(){
		$data['content']='edit_thread';
		$this->load->view('layout/backend', $data);
	}
	function threads()
	{
		$data['content']='add_thread';
		$this->load->view('layout/backend', $data);	
	}

	function get_threads()
	{		
		if($this->input->get('threadId')!=null){
			$threads=$this->generic->record_select('threads t, users u', 't.creator= u.userId and t.threadId ='.$this->input->get('threadId'),100000);
			echo json_encode($threads);
		}else{
			$threads=$this->generic->record_select('threads t, users u', 't.creator= u.userId',100000);
			echo json_encode($threads);
		}
	}

	function view_threads()
	{
		$data['content']='threads';
		$this->load->view('layout/backend', $data);	
	}
	function add_thread()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session= $this->session->userdata('sessiondata');
			$data = array(
				'title' => $this->security->xss_clean($this->input->post('title')),
				'description' => $this->security->xss_clean($this->input->post('description')),
				'creator' => $session['uid'],
				'createdOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('threads', $data))
				echo "success";
			else
				echo "failure";
		}else{
			echo "failure";
		}
	}
	function delete_thread(){
		$condition=array(
			'threadId' => $this->input->post('threadId')
			);
		if($this->generic->record_delete('threads', $condition))
			echo "success";
		else
			echo "failure";
	}

	function delete_user(){
		$condition=array(
			'userId' => $this->input->post('userId')
			);
		if($this->generic->record_delete('users', $condition))
			echo "success";
		else
			echo "failure";
	}

	function newsposts()
	{
		$data['content']='add_newspost';
		$this->load->view('layout/backend', $data);	
	}
	function view_newsposts()
	{
		$data['content']='newsposts';
		$this->load->view('layout/backend', $data);	
	}
	function get_newsposts()
	{		
		if($this->input->get('newspostId')!=null){
			$newsposts=$this->generic->record_select('newsposts', 'newspostId ='.$this->input->get('newspostId'),100000);
			echo json_encode($newsposts);
		}else{
			$newsposts=$this->generic->record_list('newsposts');
			echo json_encode($newsposts);
		}
	}
	function add_newspost()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session= $this->session->userdata('sessiondata');
			$data = array(
				'heading' => $this->security->xss_clean($this->input->post('heading')),
				'text' => $this->security->xss_clean($this->input->post('text')),
				'author' => $session['uid'],
				'publishedOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('newsposts', $data))
				echo "success";
			else
				echo "failure";
		}else{
			echo "failure";
		}
	}
	function update_newspost(){
		$newspostId=$this->uri->segment(3);
		$data=array(
			'heading'=> $this->security->xss_clean($this->input->post('heading')),
			'text'=> $this->security->xss_clean($this->input->post('text'))
			);
		if($this->generic->record_update('newsposts','newspostId ='.$newspostId,$data))
			echo "success";
		else
			echo "failure";
	}
	function update_thread(){
		$threadId=$this->uri->segment(3);
		$data=array(
			'title'=> $this->security->xss_clean($this->input->post('title')),
			'description'=> $this->security->xss_clean($this->input->post('description'))
			);
		if($this->generic->record_update('threads','threadId ='.$threadId,$data))
			echo "success";
		else
			echo "failure";
	}
	function delete_newspost(){
		$condition=array(
			'newspostId' => $this->input->post('newspostId')
			);
		// print_r($condition);
		if($this->generic->record_delete('newsposts', $condition))
			echo "success";
		else
			echo "failure";
	}

	function isUnique(){
		if($this->generic->record_check('users', 'email', $this->input->post('email')))
			echo 'unavailable';
		else
			echo "available";
	}
	function isCorrect(){
		if($this->generic->record_check('users', 'password', sha1($this->input->post('password'))))
			echo 'correct';
		else
			echo "incorrect";
	}
	function change_photo()
	{
		$data['note']='';
		$data['content']='profile';
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['max_size']             = 2000;
			$config['max_width']            = 2000;
			$config['max_height']           = 2000;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('photo'))
			{
				$file = array('upload_data' => $this->upload->data());
				$filename=$file['upload_data']['file_name'];
				$data=array(
					'photo' => $filename
					);
				if($this->generic->record_update('users','userId ='. $session['uid'] , $data))
					$data['note']="<div class='success'><i class='fa fa-check-circle'></i>Success! Work Added.</div>";
				else
					$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>Oops! Your current password was incorrect.</div>";
			}else{
				$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>" .$this->upload->display_errors('<span>','</span>')."</div>";
				$this->load->view('layout/backend', $data);
			}
			redirect('admin/profile','refresh');				
		}		
	}
	function reset_password()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'password'=> sha1($this->security->xss_clean($this->input->post('npassword')))
				);
			if($this->generic->record_update('users', 'userId ='.$session['uid'], $data))
				echo "success";
			else
				echo "failure";
		}else{
			redirect('welcome/login','refresh');	
		}	
	}

}
