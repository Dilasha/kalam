<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('generic');
	}
	public $user;
	function isSession(){
		if($this->session->userdata('sessiondata')!=null){
			$session = $this->session->userdata('sessiondata');
			$this->user = $session['uid'];
			return true;
		}
		else
			return false;
	}
	function notification_count(){
		if($this->isSession()){
			$count=$this->generic->record_count('notifications', 'seen = 0 and reciever ='.$this->user);
			return $count;
		}else
		return 0;
	}
	function index()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='home';
		$this->load->view('layout/frontend', $data);
	}
	function register()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='register';
		$this->load->view('layout/frontend', $data);
	}
	function contact()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='contact';
		$this->load->view('layout/frontend', $data);
	}
	function about()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='about';
		$this->load->view('layout/frontend', $data);
	}
	function forums(){
		$data['ncount']=$this->notification_count();
		$data['content']='forums';
		$this->load->view('layout/frontend', $data);	
	}
	function forum_thread(){
		$data['ncount']=$this->notification_count();
		$data['content']='forum_thread';
		$this->load->view('layout/frontend', $data);	
	}
	function picks(){
		$data['ncount']=$this->notification_count();
		$data['content']='picks';
		$this->load->view('layout/frontend', $data);	
	}
	function catalogue(){
		$data['ncount']=$this->notification_count();
		$data['content']='catalogue';
		$this->load->view('layout/frontend', $data);	
	}
	function book_details(){
		$data['ncount']=$this->notification_count();
		$data['content']='book_details';
		$this->load->view('layout/frontend', $data);	
	}
	function newsposts(){
		$data['ncount']=$this->notification_count();
		$data['content']='newsposts';
		$this->load->view('layout/frontend', $data);	
	}
	function newspost_full(){
		$data['ncount']=$this->notification_count();
		$data['content']='newspost_full';
		$this->load->view('layout/frontend', $data);	
	}
	function login()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='login';
		$this->load->view('layout/frontend', $data);	
	}
	function notifications(){
		$data['ncount']=$this->notification_count();
		$data['user']=$this->user;
		$data['content']='notifications';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$data['username']=$session['username'];
		}else{
			redirect('welcome/login','refresh');
		}
		$this->load->view('layout/frontend', $data);		
	}
	function profile(){
		$data['ncount']=$this->notification_count();
		$data['note']='';
		$data['content']='profile';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$data['username']=$session['username'];
		}else{
			redirect('welcome/login','refresh');
		}
		$this->load->view('layout/frontend', $data);		
	}
	function profile_edit(){
		$data['ncount']=$this->notification_count();
		$data['content']='profile_edit';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$this->load->view('layout/frontend', $data);
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function create_thread(){
		$data['ncount']=$this->notification_count();
		$data['content']='create_thread';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$this->load->view('layout/frontend', $data);
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function password_reset(){
		$data['ncount']=$this->notification_count();
		$data['content']='password_reset';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$this->load->view('layout/frontend', $data);
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function view_list()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='list';
		if($this->session->userdata('sessiondata')!=null){
			$session= $this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$this->load->view('layout/frontend', $data);
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function administrator()
	{
		$data['ncount']=$this->notification_count();
		$data['content']='administrator';
		$this->load->view('layout/blank', $data);	
	}
	function logout(){
		$data['ncount']=$this->notification_count();
		if($this->session->userdata('sessiondata')!=null){
			$session = $this->session->userdata('sessiondata');
			$data= array(
				'lastVisit'=> unix_to_human(time(), TRUE, 'eu')
				);
			$condition=array(
				'userId'=> $session['uid']
				);
			$result = $this->generic->record_update('users', $condition, $data);
		}
		$this->session->unset_userdata('sessiondata');
		$this->session->sess_destroy();

		redirect(base_url()."/welcome");
	}

}
