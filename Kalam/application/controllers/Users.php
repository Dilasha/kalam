<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('generic');		
	}
	public $user;
	function isSession(){
		if($this->session->userdata('sessiondata')!=null){
			$session = $this->session->userdata('sessiondata');
			$this->user = $session['uid'];
			return true;
		}
		else
			return false;
	}
	function isUnique(){
		if($this->generic->record_check('users', 'email', $this->input->post('email')))
			echo 'unavailable';
		else
			echo "available";
	}

	function isCorrect(){
		if($this->generic->record_check('users', 'password', sha1($this->input->post('password'))))
			echo 'correct';
		else
			echo "incorrect";
	}

	function add_thread()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session= $this->session->userdata('sessiondata');
			$data = array(
				'title' => $this->security->xss_clean($this->input->post('title')),
				'description' => $this->security->xss_clean($this->input->post('description')),
				'creator' => $session['uid'],
				'createdOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('threads', $data))
				echo "success";
			else
				echo "failure";
		}else{
			echo "failure";
		}
	}
	function send_mail()
	{
		$data = array(
			'sender' => $this->security->xss_clean($this->input->post('sender')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'subject' => $this->security->xss_clean($this->input->post('subject')),
			'message' => $this->security->xss_clean($this->input->post('msg')),
			'seen'=>0,
			'sentOn'=> unix_to_human(time(), TRUE, 'eu')
			);
		if($this->generic->record_add('messages', $data)){
			$this->load->library('email');
			
			$this->email->from($this->security->xss_clean($this->input->post('email')), $this->security->xss_clean($this->input->post('sender')));
			$this->email->to('shahdilasha@gmail.com');
			
			$this->email->subject($this->security->xss_clean($this->input->post('subject')));
			$this->email->message($this->security->xss_clean($this->input->post('msg')));
			
			$this->email->send();
			
			echo $this->email->print_debugger();
			echo "success";
		}else{
			echo "failure";
		}
	}
	function login(){
		$data = array(
			'email' => $this->security->xss_clean($this->input->post('email')),
			'password' => sha1($this->security->xss_clean($this->input->post('password'))),
			'role'=> $this->security->xss_clean($this->input->post('role'))
			);
		if($result=$this->generic->record_select('users', $data, '1')){
			$sessiondata = array(
				'username' => $result[0]->firstname.' '.$result[0]->lastname,
				'uid' => $result[0]->userId,
				'role' => $result[0]->role
				);
			$this->session->set_userdata('sessiondata', $sessiondata);
			echo "success";
		}else{
			echo "failure";
		}
	}
	function add_user(){
		$data = array(
			'lastname' => $this->security->xss_clean($this->input->post('lastname')),
			'firstname' => $this->security->xss_clean($this->input->post('firstname')),
			'email' => $this->security->xss_clean($this->input->post('email')),
			'password' => sha1($this->security->xss_clean($this->input->post('password'))),
			'dob' => $this->security->xss_clean($this->input->post('dob')),
			'photo'=> 'default.png',
			'country' => $this->security->xss_clean($this->input->post('country')),
			'role'=> $this->security->xss_clean($this->input->post('role')),
			'registeredOn' => unix_to_human(time(), TRUE, 'eu')
			);
		if($this->generic->record_add('users', $data))
			echo "success";
		else
			echo "failure";
	}
	function add_comment(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'newspostId'=> $this->input->post('newspostId'),
				'commentor'=> $session['uid'],
				'comment'=> $this->security->xss_clean($this->input->post('comment')),
				'commentedOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('comments',$data))
				echo "success";
			else
				echo "failure";
		}
	}	
	function add_list(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'listtitle'=> $this->input->post('listtitle'),
				'userId'=> $session['uid']
				);
			if($this->generic->record_add('lists',$data))
				echo "success";
			else
				echo "failure";
		}
	}	
	function add_review(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'bookId'=> $this->input->post('bookId'),
				'reviewer'=> $session['uid'],
				'review'=> $this->security->xss_clean($this->input->post('review')),
				'reviewedOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('reviews',$data))
				echo "success";
			else
				echo "failure";
		}
	}
	function delete_review(){
		
	}
	function get_rating(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$ratings = $this->generic->record_select('ratings','bookId = '.$this->input->get('bookId').' and rater ='.$session['uid'],100000);
			echo json_encode($ratings);
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function add_rating(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'bookId'=> $this->input->post('bookId'),
				'rater'=> $session['uid'],
				'rating'=> $this->security->xss_clean($this->input->post('rating'))
				);
			if($this->generic->record_add('ratings',$data))
				echo "success";
			else
				echo "failure";
			
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function add_reply(){
		if ($this->session->userdata('sessiondata')!=null) {
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'threadId'=> $this->input->post('threadId'),
				'replier'=> $session['uid'],
				'reply'=> $this->security->xss_clean($this->input->post('reply')),
				'repliedOn' => unix_to_human(time(), TRUE, 'eu')
				);
			if($this->generic->record_add('replies',$data)){
				$threaddata=array(
					'lastActive'=> unix_to_human(time(), TRUE, 'eu')
					);
				$this->generic->record_update('threads', 'threadId = '.$this->input->post('threadId') ,$threaddata);
				echo "success";
			}else
			echo "failure";
		}
	}
	function get_threads(){
		if($this->input->get('threadId')!=null){
			$threads=$this->generic->record_select('threads t, users u', 't.creator = u.userId and t.threadId = '.$this->input->get('threadId'), 1);
			echo json_encode($threads);
		}else if($this->input->get('userId')!=null){
			$threads=$this->generic->record_select('threads t, users u', 't.creator = u.userId and t.creator='.$this->input->get('userId'), 100000);
			echo json_encode($threads);
		}else {	
			$threads=$this->generic->record_select('threads t, users u', 't.creator = u.userId', 100000);
			echo json_encode($threads);
		}	
	}
	function get_books(){		
		if($this->input->get('bookId')!=null){
			$condition=array('bookId'=> $this->input->get('bookId'));
			$books=$this->generic->record_select('books', $condition, 1);
			echo json_encode($books);
		}else{	
			$books=$this->generic->record_list('books');
			echo json_encode($books);
		}
	}
	function get_newsposts(){
		if($this->input->get('newspostId')!=""){
			$newsposts=$this->generic->record_select('newsposts n, users u', 'n.author = u.userId and n.newspostId = '.$this->input->get('newspostId'), 100);
			echo json_encode($newsposts);
		}else{	
			$newsposts=$this->generic->record_select('newsposts n, users u', 'n.author = u.userId', 100000);
			echo json_encode($newsposts);
		}
	}
	function get_lists(){
		if($this->input->get('listId')!=null){
			$lists=$this->generic->record_select('lists l, users u', 'l.userId = u.userId and l.listId = '.$this->input->get('listId'), 1);
			echo json_encode($lists);
		}else if($this->input->get('userId')!=null){
			$lists=$this->generic->record_select('lists l, users u', 'l.userId = u.userId and u.userId = '.$this->input->get('userId'), 100000);
			echo json_encode($lists);
		}else{
			if($this->session->userdata('sessiondata')){
				$session=$this->session->userdata('sessiondata');
				$lists=$this->generic->record_select('lists l', 'l.userId = '.$session['uid'], 100000);
				echo json_encode($lists);
			}
		}
	}
	function get_comments(){
		$id=$this->input->get('newspostId');
		$comments=$this->generic->record_select('comments c, users u', 'c.commentor = u.userId and c.newspostId = '.$id, 10000);	
		echo json_encode($comments);
	}
	function get_users(){
		if($this->input->get('userId')!=null){
			$condition=array('userId'=> $this->input->get('userId'));
			$users=$this->generic->record_select('users', $condition, 1);
			echo json_encode($users);
		}else{	
			$users=$this->generic->record_list('users');	
			echo json_encode($users);
		}
	}
	function get_replies(){
		if($this->input->get('threadId')!=null){
			$replies=$this->generic->record_select('replies r, users u, threads t', 'r.replier = u.userId and r.threadId = t.threadId and r.threadId = '.$this->input->get('threadId'), 10000);		
			echo json_encode($replies);	
		}else if($this->input->get('userId')!=null)
		{
			$replies=$this->generic->record_select('replies r, users u, threads t', 'r.replier = u.userId and t.threadId = r.threadId and u.userId ='.$this->input->get('userId'), 10000);	
			echo json_encode($replies);	
		}
	}
	function get_reviews()
	{
		if($this->input->get('bookId')!=null){
			$reviews=$this->generic->record_select('reviews r, users u', 'r.reviewer = u.userId and r.bookId = '.$this->input->get('bookId'), 10000);		
			echo json_encode($reviews);	
		}else if($this->input->get('userId')!=null)
		{
			$reviews=$this->generic->record_select('reviews r, users u, books b', 'r.reviewer = u.userId and b.bookId = r.bookId and u.userId ='.$this->input->get('userId'), 10000);	
			echo json_encode($reviews);	
		}
	}
	function edit_profile()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data = array(
				'lastname' => $this->security->xss_clean($this->input->post('lastname')),
				'firstname' => $this->security->xss_clean($this->input->post('firstname')),
				'dob' => $this->security->xss_clean($this->input->post('dob')),
				'country' => $this->security->xss_clean($this->input->post('country')),
				'bio' => $this->security->xss_clean($this->input->post('bio')),
				);
			if($this->generic->record_update('users', 'userId ='.$session['uid'] ,$data))
				echo "success";
			else
				echo "failure";
		}else{
			redirect('welcome/login','refresh');
		}
	}
	function get_notifications(){
		if($this->isSession()){				
			if($result=$this->generic->record_select('notifications', 'reciever ='.$this->user, 1000))
				echo json_encode($result);
		}
	}
	function notif_seen(){
		if($this->isSession()){
			$data=array('seen'=>1);
			$notifications = $this->generic->record_update('notifications', 'seen = 0 and reciever ='.$this->user,$data);
			echo json_encode($notifications);
		}
	}
	function change_photo()
	{
		$data['note']='';
		$data['content']='profile';
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data['user']=$session['uid'];
			$config['upload_path']          = './uploads/';
			$config['allowed_types']        = 'jpg|jpeg|png';
			$config['max_size']             = 2000;
			$config['max_width']            = 2000;
			$config['max_height']           = 2000;

			$this->load->library('upload', $config);

			if ($this->upload->do_upload('photo'))
			{
				$file = array('upload_data' => $this->upload->data());
				$filename=$file['upload_data']['file_name'];
				$data=array(
					'photo' => $filename
					);
				if($this->generic->record_update('users','userId ='. $session['uid'] , $data))
					$data['note']="<div class='success'><i class='fa fa-check-circle'></i>Success! Work Added.</div>";
				else
					$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>Oops! Your current password was incorrect.</div>";
			}else{
				$data['note']="<div class='failure'><i class='fa fa-exclamation-triangle'></i>" .$this->upload->display_errors('<span>','</span>')."</div>";
				$this->load->view('layout/frontend', $data);
			}
			redirect('welcome/profile','refresh');				
		}		
	}
	function reset_password()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'password'=> sha1($this->security->xss_clean($this->input->post('npassword')))
				);
			if($this->generic->record_update('users', 'userId ='.$session['uid'], $data))
				echo "success";
			else
				echo "failure";
		}else{
			redirect('welcome/login','refresh');	
		}	
	}
	function add_to_list()
	{
		if($this->session->userdata('sessiondata')!=null)
		{
			$session=$this->session->userdata('sessiondata');
			$data=array(
				'bookId'=> $this->security->xss_clean($this->input->post('bookId')),
				'listId'=> $this->security->xss_clean($this->input->post('listId'))
				);
			if($this->generic->record_add('book_lists', $data))
				echo "success";
			else
				echo "failure";
		}else{
			redirect('welcome/login','refresh');	
		}	
	}
	function delete_list(){
		$condition=array(
			'listId' => $this->input->post('listId')
			);
		if($this->generic->record_delete('lists', $condition))
			echo "success";
		else
			echo "failure";
	}
	function delete_notification(){
		$condition=array(
			'notificationId' => $this->input->post('notificationId')
			);
		if($this->generic->record_delete('notifications', $condition))
			echo "success";
		else
			echo "failure";
	}
	function delete_thread(){
		$condition=array(
			'threadId' => $this->input->post('threadId')
			);
		if($this->generic->record_delete('threads', $condition))
			echo "success";
		else
			echo "failure";
	}
	function delete_listbook(){
		$condition=array(
			'booklistId' => $this->input->post('booklistId')
			);
		// print_r($condition);
		if($this->generic->record_delete('book_lists', $condition))
			echo "success";
		else
			echo "failure";
	}	
	function get_listbooks()
	{
		if($this->input->get('listId')!=null){
			$listbooks=$this->generic->record_select('book_lists bl, books b, lists l', 'bl.bookId=b.bookId and bl.listId=l.listId and l.listId = '.$this->input->get('listId'), 10000);		
			echo json_encode($listbooks);	
		}
	}
}