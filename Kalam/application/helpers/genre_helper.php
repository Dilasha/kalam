<?php
if ( ! function_exists('get_genres'))
{
	// custom helper to return an array of genres available
	function get_genres()
	{
		$genres = array(
			'Action and Adventure' => 'Action and Adventure',
			'Biography and Autobiography' => 'Biography and Autobiography',
			'Comedy and Humour' => 'Comedy and Humour',
			'Comics and Manga' => 'Comics and Manga',
			'Crime and Detective' => 'Crime and Detective',
			'Dystopia and Eutopia' => 'Dystopia and Eutopia',
			'Fantasy' => 'Fantasy',
			'Fiction' => 'Fiction',
			'Graphic Novels' => 'Graphic Novels',
			'Historical Fiction' => 'Historical Fiction',
			'History' => 'History',
			'Middle Grade and Children' => 'Middle Grade and Children',
			'Mystery' => 'Mystery',
			'Non-fiction' => 'Non-fiction',
			'Picture Books' => 'Picture Books',
			'Poetry and Plays' => 'Poetry and Plays',
			'Realistic Fiction' => 'Realistic Fiction',
			'Religion and Mythology' => 'Religion and Mythology',
			'Romance and Drama' => 'Romance and Drama',
			'Science-Fiction' => 'Science-Fiction',
			'Short Stories and Collections' => 'Short Stories',
			'Thriller' => 'Thriller',
			'Young Adult'=>'Young Adult'
			);
		return $genres;
	}
}
?>