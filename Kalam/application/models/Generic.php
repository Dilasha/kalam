<?php
class Generic extends CI_Model{
	function __construct() {
		parent::__construct();
	}

	function record_add($table, $data)
	{
		if($this->db->insert($table, $data))
			return true;
		else
			return false;
	}
	function record_update($table, $condition, $data){
		$this->db->where($condition);
		if($this->db->update($table, $data))
			return true;
		else
			return false;		
	}
	function record_check($table, $column, $data)
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($column,$data);
		$this->db->limit(1);
		return $this->db->get()->result();
	}
	function record_select($table, $condition, $limit){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($condition);
		$this->db->limit($limit);
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return false;
	}
	function record_list($table){
		$this->db->select('*');
		$this->db->from($table);
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->result();
		else
			return false;
	}
	function record_delete($table, $condition){		
		$this->db->where($condition);
		if($this->db->delete($table))
			return true;
		else
			return false;
	}
	function record_count($table, $condition){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->num_rows();
	}
}
?>
